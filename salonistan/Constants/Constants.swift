    //
//  Constants.swift
//  salonistan
//
//  Created by Osama Ahmed on 08/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit
    
let screenSize : CGRect = UIScreen.main.bounds;
let _window: UIWindow! = UIApplication.shared.delegate!.window!

    
struct cons {
    static let secure = "s";
    static let domain = "beautybells.pk/api";
    static let baseurl = "beautybells.pk/public"
}

struct api {
    static let SERVICEURL = "http\(cons.secure)://\(cons.domain)/";
    static let IMAGEURL = "http\(cons.secure)://\(cons.baseurl)/";
    static let Login = "login";
    static let Forget = "user/forgetpassword";
    static let Signup = "signup"
    static let GetProfile = "getuser"
    static let EditProfile = "updateuser"
    static let Categories = "categories"
    static let Banners = "banners"
    static let search = "search"
    static let servicesOfCategory = "services-of-category";
    static let salonsOfService = "salons-of-service";
    static let featuresOfSalon = "features-of-salons";
    static let featuresOfStylist = "features-of-stylists";
    static let SalonsByFilter = "get-salons-by-filter";
    static let getBothAppointments = "get-both-appointments";
    static let setAppointment = "set-appointment"
    static let getAnAppointment = "get-an-appointment"
    static let getShopReviews = "reviews"
    static let getStylistReviews = "reviews"
    static let setShopReviews = "add-style-review"
    static let setStylistReviews = "add-freelancer-review"
    
    
    
    
}
    
struct  Colors {
    static let gradientFirstColorGreen = "#00a185";
    static let gradientSecondColorGreen = "#219271";
    static let themeGreen = "#00a185";
    static let twitterBlue = "#1da1f2";
    static let facebookBlue = "#4267b2";
    
    
    static let blue = "#2398C2";
    static let bluebtn = "#289cd6";
    static let bluetxt = "#24a7e8";
    static let bluebg = "#1b2046";
    
    static let black = "#000000";
    static let lightgray = "#E3E3E3";
    static let lightgray2 = "#f1f1f1";
    static let gray = "#9B9B9B";
    static let white = "#FFFFFF";
    static let green = "#458E51";
    static let red = "#CD2C33";
    static let maroon = "#CD2C33";
    static let yellow = "b06814";
    static let brown = "#4A3232"
    static let randoms = ["#e6194b", "#3cb44b", "#ffe119", "#4363d8", "#f58231", "#911eb4", "#46f0f0", "#f032e6", "#bcf60c", "#fabebe", "#008080", "#e6beff", "#9a6324", "#fffac8", "#800000", "#aaffc3", "#808000", "#ffd8b1", "#000075", "#808080", "#ffffff", "#000000"]
    
    
    
}
    
struct settings
{
    static let timeOut :Double = 5.0;
    static let gPassword : String = "EFAFBF4D";
    static let btnHeight = 45;
}
    
    
    
    
    
    
    
    
    
    
    
