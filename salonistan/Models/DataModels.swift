//
//  DataModels.swift
//  salonistan
//
//  Created by Osama Ahmed on 07/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import EVReflection

open class AppointmentDetailModel: EVObject{
    var id: NSNumber!
    var salonId: NSNumber!
    var stylistId: NSNumber!
    var clientId: NSNumber!
    var serviceIds: String!
    var staffId: NSNumber!
    var date: String!
    var time: String!
    var duration: String!
    var price: String!
    var payment_method: NSNumber!
    var statusId: NSNumber!
    var createdDate: String!
    var updatedDate: String!
    var deletedDate: String!
    var servicesNames: [String]!
    var associated_services: [AssociatedService]!
    var get_client_user: UserModel!
    var get_staff: StylistDetailModel!
    var get_stylist: StylistDetailModel!
    var get_salon: SalonModel!
}


open class AppointmentModel: EVObject{
    var id: NSNumber!
    var salonId: NSNumber!
    var stylistId: NSNumber!
    var clientId: NSNumber!
    var serviceIds: String!
    var staffId: NSNumber!
    var date: String!
    var time: String!
    var duration: String!
    var price: String!
    var payment_method: NSNumber!
    var statusId: NSNumber!
    var createdDate: String!
    var updatedDate: String!
    var deletedDate: String!
    var isStylist: NSNumber!
    var servicesNames: [String] = [String]()
    var get_client_user: UserModel!
    var get_staff: StylistModel!
    var get_stylist: StylistDetailModel!
    var get_salon: SalonModel!
}


open class EnSignUp: EVObject {
    var password: String? = settings.gPassword;
    var Id: String? = ""
    var Name: String? = ""
    var Gender: String? = ""
    var Email: String? = ""
    var username: String? = ""
    var Token: String? = ""
    var FacebookId: String? = ""
    var ImageUrl: String? = ""
}

open class StylistModel: EVObject{
    var id: NSNumber!
    var stylistId: NSNumber!
    var serviceId: NSNumber!
    var serviceCost: String!
    var serviceDuration: String!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var updatedBy: NSNumber!
    var createdDate: String!
    var updatedDate: String!
    var deletedDate: String!
    var created_at: String!
    var updated_at: String!
    var deleted_at: String!
    var reviews: [ReviewModel] = [ReviewModel]()
    var reviewCount: NSNumber!
    var avgrating: NSNumber!
    var get_stylist: StylistDetailModel!
}

open class StylistDetailModel: EVObject{
    var id: NSNumber!
    var stylistId: NSNumber!
    var serviceId: NSNumber!
    var serviceCost: String!
    var serviceDuration: String!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var updatedBy: NSNumber!
    var createdDate: String!
    var updatedDate: String!
    var deletedDate: String!
    var created_at: String!
    var updated_at: String!
    var deleted_at: String!
    var reviews: [ReviewModel] = [ReviewModel]()
    var reviewCount: NSNumber!
    var avgrating: NSNumber!
    var get_stylist: StylistDetailModel!
    var banners: [BannerModel] = [BannerModel]()
    var name: String!
    var Description: String!
    var thumbnail: String!
    var image: String!
    var categoryIds: String!
    var specialityId: NSNumber!
    var postalcode: String!
    var address: String!
    var street: String!
    var rating: String!
    var latitude: String!
    var longitude: String!
    var minReservationTime: NSNumber!
    var userId: NSNumber!
    var isAvailable: NSNumber!
    var notAvailableFrom: String!
    var notAvailableTo: String!
    var disableDate: String!
    var disableTime: String!
    var enableDate: String!
    var enableTime: String!
    var rank: NSNumber!
    var cityId: NSNumber!
    var areaId: NSNumber!
    var isTiming: TimingsModel!
    var booking: NSNumber!
    var pending: NSNumber!
    var confirm: NSNumber!
    var complete: NSNumber!
    var cancel: NSNumber!
    var barberCancel: NSNumber!
    var declined: NSNumber!
    var notArrived: NSNumber!
    var autoCancel: NSNumber!
    var salon: String!
    var salonId: NSNumber!
    var associated_services: [AssociatedService] = [AssociatedService]()
    var categoryNames: [String] = [String]()
    var speciality: SpecialityModel!
    var albums: [AlbumModel] = [AlbumModel]()
}

open class SpecialityModel: EVObject{
    var id: NSNumber!
    var name: String!
    var Description: String!
    var rank: NSNumber!
    var created_at: String!
    var updated_at: String!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
}

open class TimingsModel: EVObject{
    var id: NSNumber!
    var barbershopId: NSNumber!
    var stylistId: NSNumber!
    var day: NSNumber!
    var timeFrom: String!
    var timeTo: String!
    var breakTimeFrom: String!
    var breakTimeTo: String!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var deletedDate: String!
    var created_at: String!
    var updated_at: String!
}









open class SalonModel: EVObject{
    var id: NSNumber!
    var userId: NSNumber!
    var name: String!
    var Description: String!
    var categoryIds: String!
    var postalcode: String!
    var address: String!
    var street: String!
    var areaId: NSNumber!
    var cityId: NSNumber!
    var image: String!
    var rating: String!
    var latitude: String!
    var longitude: String!
    var rank: NSNumber!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var minReservationTime: NSNumber!
    var phone_number: String!
    var facebook: String!
    var google: String!
    var linkdin: String!
    var skype: String!
    var website: String!
    var email: String!
    var booking: NSNumber!
    var pending: NSNumber!
    var confirm: NSNumber!
    var complete: NSNumber!
    var cancel: NSNumber!
    var barberCancel: NSNumber!
    var declined: NSNumber!
    var notArrived: NSNumber!
    var autoCancel: NSNumber!
    var categoryNames: [String] = [String]()
    var reviewCount: NSNumber!
    var avgrating: NSNumber!
    var reviews: [ReviewModel] = [ReviewModel]()
    var banners: [BannerModel] = [BannerModel]()
    var city: idName!
    var area: areaModel!
    var timings: [TimingsModel] = [TimingsModel]();
    var associated_facilities: [AssociatedFacility] = [AssociatedFacility]()
    var associated_services: [AssociatedService] = [AssociatedService]()
    var associated_stylists: [AssociatedStylist] = [AssociatedStylist]()
    var albums: [AlbumModel] = [AlbumModel]()
    
    
    
}

open class AlbumModel: EVObject{
    var id: NSNumber!
    var barbershopId: NSNumber!
    var stylistId: NSNumber!
    var name: String!
    var thumbnail: String!
    var image: String!
    var Description: String!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var updatedBy: NSNumber!
    var updated_at: String!
    var created_at: String!
    var createdDate: String!
    var updatedDate: String!
    var deletedDate: String!
    var photos: [PhotoModel] = [PhotoModel]()
}

open class PhotoModel: EVObject{
    var id: NSNumber!
    var barbershopId: NSNumber!
    var stylistId: NSNumber!
    var albumId: NSNumber!
    var caption: String!
    var captionAr: String!
    var thumbnail: String!
    var image: String!
    var rank: NSNumber!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var Description: String!
    var isDelete: NSNumber!
}



open class AssociatedStylist: EVObject{
    var id: NSNumber!
    var barbershopId: NSNumber!
    var stylistId: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var isDelete: NSNumber!
    var get_stylist: StylistDetailModel!
}

open class AssociatedService: EVObject{
    var id: NSNumber!
    var barbershopId: NSNumber!
    var stylistId: NSNumber!
    var serviceId: NSNumber!
    var serviceCost: String!
    var serviceDuration: String!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var categoryId: NSNumber!
    var isDelete: NSNumber!
    var isPromotion: NSNumber!
    var service: ServiceModel!
}

open class AssociatedFacility: EVObject{
    var id: NSNumber!
    var barbershopId: NSNumber!
    var facilitiesId: NSNumber!
    var distance: String!
    var remarks: String!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var isDelete: NSNumber!
    var get_facility: String!
}

open class FacilityModel: EVObject{
    var id: NSNumber!
    var name: String!
    var icon: String!
    var rank: NSNumber!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var isDelete: NSNumber!
}



open class ReviewModel: EVObject{
    var id: NSNumber!
    var barbershopId: NSNumber!
    var userId: NSNumber!
    var styleId: NSNumber!
    var subject: String!
    var message: String!
    var rating: NSNumber!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var isDelete: NSNumber!
}

open class ServiceModel: EVObject{
    var id: NSNumber!
    var name: String!
    var Description: String!
    var categoryId: NSNumber!
    var icon: String!
    var rank: NSNumber!
    var statusId: NSNumber!
    var isPromotion: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var isDelete: NSNumber!
    var associated_service: AssociatedService!
}

open class areaModel: EVObject{
    var id: NSNumber!
    var name: String!
    var cityId: NSNumber!
}

open class idName: EVObject{
    var id: NSNumber!
    var name: String!
}

open class BannerModel: EVObject {
    
    var id: NSNumber!
    var barbershopId: NSNumber!
    var stylistId: NSNumber!
    var image: String!
    var caption: String!
    var urlLink: String!
    var urlType: String!
    var startDate: NSNumber!
    var endDate: NSNumber!
    var language: String!
    var rank: NSNumber!
    var statusId: NSNumber!
    var createdBy: NSNumber!
    var createdDate: String!
    var updatedBy: NSNumber!
    var updatedDate: String!
    var promotion: String!
    var isDelete: NSNumber!
    
}



open class CategoryModel: EVObject {
    
    var id: NSNumber!
    var name: String!
    var image: String!
    var createdDate: String!
    var updatedDate: String!
    var isDelete: NSNumber!
    
}

open class SignupModel: EVObject{
    
    var email: String!
    var password: String!
    var first_name: String!
    var last_name: String!
    var gender: String!
    var mobileNo: String!
    var special_offers: Bool = false
    var image: String!
    
}


open class UserModel: EVObject {
    
    var id: NSNumber!
    var password: String!
    var roleId: NSNumber!
    var firstName: String!
    var lastName: String!
    var username: String!
    var profilePic: String!
    var passwordDate: String!
    var gender: NSNumber!
    var mobileNo: String!
    var email: String!
    var language: String!
    var nationality: String!
    var emirates: String!
    var sendAlerts: String!
    var displayPic: String!
    var statusId: NSNumber!
    var createdBy: String!
    var createdDate: String!
    var updatedBy: String!
    var updatedDate: String!
    var emailverify: NSNumber!
    var calender: String!
    var specialOffers: NSNumber!
    
}

class ResData: EVObject{
    var error: NSNumber? = 0;
    var data: NSDictionary = [:]
    var message: String = ""
    var utc: String!
    
}

class ResArrayData: EVObject{
    var error: NSNumber? = 0;
    var data: [NSDictionary] = [[:]]
    var message: String = ""
    var utc: String!
}
