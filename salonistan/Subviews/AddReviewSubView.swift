//
//  AddReviewSubView.swift
//  salonistan
//
//  Created by Osama Ahmed on 21/02/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

@objc protocol AddReviewSubViewDelegate{
    func submitRefreshView(view: AddReviewSubView, doRefresh: Bool );
    func submitReview(view: AddReviewSubView, post: [String:Any])
}

class AddReviewSubView: UIView {
    //
    @IBOutlet weak var starReview: CosmosView!
    @IBOutlet weak var lblLovedIt: UILabel!
    @IBOutlet weak var comments: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var outerView: UIView!
    
    var starArray: [String] = ["Dislike it", "It's OK", "Liked it", "Loved it", "Loved it"];
    var reviewId: Int = 3;
    var addedRating: Int = 4;
    weak var delegate: AddReviewSubViewDelegate?
    
    override func awakeFromNib() {
        //
        
        self.lblLovedIt.text = starArray[4];
        self.comments.setBorderBottom(color: Colors.red, height: 1)
        btnSubmit.ThemeType1();
        outerView.setBorder(color: Colors.red, radius: 10, width: 1)
        starReview.didFinishTouchingCosmos = { rating in
            self.setRating(rating)
        }
    }
    
    func setRating(_ rating: Double ){
        self.starReview.rating = rating;
        let index = Int(ceil(rating)) - 1;
        self.lblLovedIt.text = starArray[index];
        self.addedRating = index;
    }
    
    @IBAction func addReviewView(){
        
        
        let commentns: String = comments.text! as String;
        
        //doing Validations
        if(commentns.count == 0){return;}
        guard let user = UserDataHelper.returnUser() else { return }
        
        
        var post = [
            "userId": Int(user.id),
            "subject":starArray[addedRating],
            "message":commentns,
            "rating": addedRating
        ] as! [String:Any]
        
        print(post);
        delegate?.submitReview(view: self, post: post);
        
    }
    
    @IBAction func closeReviewView(){
        delegate?.submitRefreshView(view: self, doRefresh: false);
    }
    
    
    
    
    
    
}

