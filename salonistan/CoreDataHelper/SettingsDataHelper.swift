//
//  SettingsDataHelper.swift
//  Celebrity
//
//  Created by Xtreme Hardware on 22/07/2018.
//  Copyright © 2018 pixel. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SettingsDataHelper{
    
    private static var entityName = "Settings";
    // create
    class func createSettings(_ skip: Bool, completion: @escaping (_ success: NSManagedObject?) -> Void){
            
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        let item = NSManagedObject(entity: entity!, insertInto: context) as! Settings;
        item.setValue(skip, forKey: "skipTutorial");
        
        
        do{
            
            try context.save();
            completion(item);
            
        } catch {
            
            print("Failed")
            completion(nil);
        }
        
        
    }
    
    // update
    class func updateSettings(_ skip: Bool, completion: @escaping (_ success: NSManagedObject?) -> Void){
        // not required but may be in future
        
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName);
        request.returnsObjectsAsFaults = false;
        
        
        do {
            let result = try context.fetch(request).first as! Settings;
            if(result != nil){
                result.skipTutorial = true;
                
                
                try context.save();
                completion(result);
            }else{
                
                self.createSettings(skip) { (res) in
                    completion(res);
                }
                
            }
                
            
        } catch {
            
            print("Failed")
            completion(nil);
        }
    }
    
    // return
    class func returnSettings() ->NSManagedObject?{
        
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName);
        request.returnsObjectsAsFaults = false;
        request.fetchLimit = 1
        
        do {
            
            let result = try context.fetch(request).first;
            return result as? NSManagedObject;
            
        } catch {
            
            print("Failed")
            return nil;
        }
        
    }
    
    
    
    // Check
    class func checkIfSettingsExist() -> Bool{
        
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName);
        request.returnsObjectsAsFaults = false;
        request.fetchLimit = 1
        
        do {
            
            let c = try context.count(for: request);
            if( c == 0){
                return false;
            }else{
                return true;
            }
            
        } catch {
            
            print("Failed")
            return false;
        }
        
        
    }
    
    class func skipTutorial(_ skip: Bool) -> Bool{
        // not required but may be in future
        
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName);
        request.returnsObjectsAsFaults = false;
        
        
        do {
            let result = try context.fetch(request).first as! Settings;
            if(result != nil){
                result.skipTutorial = skip;
                try context.save();
                return true;
            }else{
                return false;
            }
            
            
        } catch {
            
            print("Failed")
            return false;
        }
    }
    
    
    
}
