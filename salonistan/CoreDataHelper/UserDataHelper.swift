//
//  UserDataHelper.swift
//  gpsfirst
//
//  Created by Osama Ahmed on 26/09/2018.
//  Copyright © 2018 reaverb. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class UserDataHelper{
    
    private static var entityName = "Users";
    
    // create
    class func createUser(_ user: UserModel, completion: @escaping (_ success: Users?) -> Void){
        
        // if user exist update
        // else create
        
        if(self.checkIfUserExist()){
            self.update(user) { (obj) in
                completion(obj)
            }
        }else{
            self.create(user) { (obj) in
                completion(obj)
            }
        }
        
        
        
    }
    
    // create
    class func create(_ user: UserModel, completion: @escaping (_ success: Users?) -> Void){
        
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        let item = NSManagedObject(entity: entity!, insertInto: context) as! Users;
        
        let e = user.toDictionary() as! [String:Any];
        item.setValuesForKeys(e)
        
        
        
        do{
            try context.save();
            completion(item);
            
        } catch {
            
            print("Failed")
            completion(nil);
        }
        
    }
    
    // update
    class func update(_ user: UserModel, completion: @escaping (_ success: Users?) -> Void){
        // not required but may be in future
        
        let email = user.email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName);
        request.returnsObjectsAsFaults = false;
        request.fetchLimit = 1
        
        do {
            
            let result = try context.fetch(request).first;
            var iuser = result as? Users
            
            let e = user.toDictionary() as! [String:Any];
            iuser?.setValuesForKeys(e)
            
            
            try context.save();
            completion(iuser)
            
            
        } catch {
            
            print("Failed")
            completion(nil)
        }
        
        
        
    }
    
    class func setLogout() -> Bool{
        
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName);
        request.returnsObjectsAsFaults = false;
        request.fetchLimit = 1
        
        do {
            
            let c = try context.count(for: request);
            if( c <= 0){
                return true;
            }else{
                
                let result = try context.fetch(request).first;
                let iuser = result as? Users
                context.delete(iuser!);
                try context.save();
                
                return true;
            }
            
        } catch {
            
            print("Failed")
            return false;
        }
        
        
    }
    
    class func returnUser() -> Users!{
        
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName);
        request.returnsObjectsAsFaults = false
        
        var firstElement: Users!;
        
        do {
            let result = try context.fetch(request) as! [Users];
            if(result.count == 0){
                return nil
            }else{
                firstElement = result.first
            }
            
        } catch {
            
            print("Failed")
            
        }
        
        return firstElement;
        
    }
    
    class func checkIfUserExist() -> Bool{
        
        let appDelegate =  AppDelegate.getAppDelegate();
        let context = appDelegate.persistentContainer.viewContext;
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName);
        request.returnsObjectsAsFaults = false;
        request.fetchLimit = 1
        
        do {
            
            let c = try context.count(for: request);
            if( c <= 0){
                return false;
            }else{
                return true;
            }
            
        } catch {
            
            print("Failed")
            return false;
        }
        
        
    }
}
