//
//  ApiHelper.swift
//  gpsfirst
//
//  Created by Osama Ahmed on 24/09/2018.
//  Copyright © 2018 reaverb. All rights reserved.
//

import Foundation
import UIKit

class ApiHelper {
    //
    
    class func ChangePassword(_ oldpassword: String, _ newpassword: String, completion: @escaping (_ callback: Bool) -> Void){
        
        guard let userid = UserDataHelper.returnUser()?.id, userid != nil else {
            completion(false);
            return
        }
        
        
        // Post Model Create
        let post = [
            "old_password": "\(oldpassword)",
            "new_password": "\(newpassword)",
        ]
        
        //Network Request
        NetworkHelper.MakePostRequest("user/\(userid)/password", postData: post, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            UtilityHelper.AlertMessage("Password updated successfully");
            completion(true);
            
            
        },failure: { (error) -> Void in
            
            print("ERROR");
            if let _err = error{
                UtilityHelper.AlertMessage(_err["message"] as! String)
            };
            completion(false);
            
        })
        
        
    }
    
    class func ForgetPassword(_ email: String, completion: @escaping (_ callback: Bool) -> Void){
        
        //Network Request
        NetworkHelper.MakeGetRequest("\(api.Forget)/\(email)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! NSDictionary
            UtilityHelper.AlertMessage(data["status"] as! String)
            completion(true);
            
            
        },failure: { (error) -> Void in
            
            let _e = error as! NSDictionary
            UtilityHelper.AlertMessage(_e["message"] as! String)
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false);
            
        })
        
        
    }
    
    class func Login(_ email: String, _ password: String, completion: @escaping (_ callback: Bool) -> Void){
        
        // Post Model Create
        let post = [
            "email": "\(email)",
            "password": "\(password)",
        ]
        
        let userModel = UserModel();
        userModel.email = email;
        userModel.password = password;
        
        
        //Network Request
        NetworkHelper.MakePostRequest("\(api.Login)", postData: post, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! NSDictionary
            let userModel = UserModel(dictionary: data);

            UserDataHelper.createUser(userModel, completion: { (user) in
                //
                completion(true);
            })
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false);
            
        })
        
        
    }
    
    class func Signup(_ model: SignupModel, completion: @escaping (_ callback: Bool) -> Void){
        
        // Post Model Create
        let post = model.toDictionary() as! [String : Any];
        
        //Network Request
        NetworkHelper.MakePostRequest("\(api.Signup)", postData: post, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! NSDictionary
            let userModel = UserModel(dictionary: data);
            
            UserDataHelper.createUser(userModel, completion: { (user) in
                //
                completion(true);
            })
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false);
            
        })
        
        
    }
    
    class func GetProfile( completion: @escaping (_ callback: Bool, _ data: UserModel? ) -> Void){
    
        // Post Model Create
        guard let user = UserDataHelper.returnUser() else { completion(false, nil); return}
        
        //Network Request
        //let _id =
        NetworkHelper.MakeGetRequest("\(api.GetProfile)/\(user.id)", postData: nil, showLoader: true, success: { (successData) -> Void in
        
            let data = successData as! NSDictionary
            let userModel = UserModel(dictionary: data);
            completion(true, userModel);
        
        
        },failure: { (error) -> Void in
        
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil);
        
        })
    
    
    }
    
    class func EditProfle(_ model: [String:Any], completion: @escaping (_ callback: Bool) -> Void){
        
        // Post Model Create
        guard let user = UserDataHelper.returnUser() else { completion(false); return}
        let post = model
        
        //Network Request
        NetworkHelper.MakePostRequest("\(api.EditProfile)/\(user.id)", postData: post, showLoader: true, success: { (successData) -> Void in
            
            completion(true);
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false);
            
        })
        
        
    }
    
    class func Categories(completion: @escaping (_ callback: Bool, _ data: [CategoryModel]? ) -> Void){
        
        //Network Request
        NetworkHelper.MakeGetRequest("\(api.Categories)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! NSDictionary
            let categories = [CategoryModel](dictionaryArray: data["categories"] as! [NSDictionary] );
            completion(true, categories)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    class func Banners(completion: @escaping (_ callback: Bool, _ data: [BannerModel]? ) -> Void){
        
        //Network Request
        NetworkHelper.MakeGetRequestForArray("\(api.Banners)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! [NSDictionary]
            let banners = [BannerModel](dictionaryArray: data);
            completion(true, banners)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    class func Search(text:String, completion: @escaping (_ callback: Bool, _ category: [CategoryModel]?, _ service: [ServiceModel]? , _ salon: [SalonModel]? ) -> Void){
        
        //Network Request
        let url = text
        
        NetworkHelper.MakeGetRequest("\(api.search)/\(url)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! NSDictionary
            
            let category = [CategoryModel](dictionaryArray: data["category"] as! [NSDictionary]);
            let service = [ServiceModel](dictionaryArray: data["servic"] as! [NSDictionary]);
            let salon = [SalonModel](dictionaryArray: data["salon"] as! [NSDictionary]);
            
            
            completion(true, category, service, salon)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil, nil, nil)
            
        })
        
        
    }
    
    class func ServicesOfCategory(id: Int, completion: @escaping (_ callback: Bool, _ service: [ServiceModel]? ) -> Void){
        
        //Network Request
        NetworkHelper.MakeGetRequest("\(api.servicesOfCategory)/\(id)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData! as NSDictionary
            
            let service = [ServiceModel](dictionaryArray: data["services"] as! [NSDictionary]);
            
            
            completion(true, service)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    class func SalonsOfService(id:Int, completion: @escaping (_ callback: Bool, _ salon: [SalonModel]?, _ stylist: [StylistModel]? ) -> Void){
        
        //Network Request
        NetworkHelper.MakeGetRequest("\(api.salonsOfService)/\(id)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! NSDictionary
            
            var stylist = [StylistModel](dictionaryArray: data["stylists"] as! [NSDictionary]);
            
            let index = stylist.index{ $0.get_stylist == nil };
            if let _i = index, _i != nil{
                stylist.remove(at: _i);
            }
            
            
            
            let salon = [SalonModel](dictionaryArray: data["salons"] as! [NSDictionary]);
            
            
            completion(true, salon, stylist)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil, nil)
            
        })
        
        
    }
    
    class func featuresOfSalon(id:Int, completion: @escaping (_ callback: Bool, _ salon: SalonModel? ) -> Void){
        
        //Network Request
        NetworkHelper.MakeGetRequest("\(api.featuresOfSalon)/\(id)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! NSDictionary
            var salon = SalonModel(dictionary: data as! NSDictionary);
            
            completion(true, salon)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    class func featuresOfStylist(id:Int, completion: @escaping (_ callback: Bool, _ salon: StylistDetailModel? ) -> Void){
        
        //Network Request
        NetworkHelper.MakeGetRequest("\(api.featuresOfStylist)/\(id)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! NSDictionary
            var stylist = StylistDetailModel(dictionary: data as! NSDictionary);
            
            completion(true, stylist)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    class func SalonsByFilter(id:Int, filter: String, completion: @escaping (_ callback: Bool, _ salon: [SalonModel]? ) -> Void){
        
        // Post Model Create
        var post = [
            "filter": filter
//            "latitude": 24.911865,
//            "longitude": 67.102438
            ] as [String : Any]
        
        if filter == "nearest", let lo = LocationHelper.shared.locationManager.location?.coordinate{
            post["latitude"] = lo.latitude;
            post["longitude"] = lo.longitude
        };
        
        
        //Network Request
        NetworkHelper.MakePostRequestForArray("\(api.SalonsByFilter)/\(id)", postData: post, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! [NSDictionary]
            let salon = [SalonModel](dictionaryArray: data as! [NSDictionary]);
            
            completion(true, salon)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    class func GetCurrentAppointments(completion: @escaping ( _ callback: Bool, _ appointments: [AppointmentModel]? ) -> Void){
        
        guard let id = UserDataHelper.returnUser()?.id else { return }
        
        //Network Request
        NetworkHelper.MakeGetRequest("\(api.getBothAppointments)/\(id)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData! as NSDictionary
            let app = [AppointmentModel](dictionaryArray: data["current"] as! [NSDictionary]);
            
            completion(true, app)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    class func GetPreviousAppointments(completion: @escaping ( _ callback: Bool, _ appointments: [AppointmentModel]? ) -> Void){
        
        guard let id = UserDataHelper.returnUser()?.id else { return }
        
        //Network Request
        NetworkHelper.MakeGetRequest("\(api.getBothAppointments)/\(id)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData as! NSDictionary
            var app = [AppointmentModel](dictionaryArray: data["previous"] as! [NSDictionary]);
            
            completion(true, app)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    class func SetAppointment(shopid: Int, data: [String:Any], completion: @escaping ( _ callback: Bool ) -> Void){
        
        //Network Request
        NetworkHelper.MakePostRequest("\(api.setAppointment)/\(shopid)", postData: data, showLoader: true, success: { (successData) -> Void in
            
            completion(true)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false)
            
        })
        
        
    }
    
    class func GetAppointment(id: Int, type: String, completion: @escaping ( _ callback: Bool, _ ap: AppointmentDetailModel? ) -> Void){
        
        // Network Request
        NetworkHelper.MakeGetRequest("\(api.getAnAppointment)/\(type)/\(id)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData! as NSDictionary
            let app = AppointmentDetailModel(dictionary: data );
            
            completion(true, app)
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    class func GetReviews(id: Int, type: String, completion: @escaping ( _ callback: Bool, _ ap: [ReviewModel]? ) -> Void){
        
        let api_string = (type == "stylist") ? api.getStylistReviews : api.getShopReviews
        
        
        
        // Network Request
        NetworkHelper.MakeGetRequestForArray("\(api_string)/\(id)", postData: nil, showLoader: true, success: { (successData) -> Void in
            
            print(successData!);
            let data = successData! as [NSDictionary]
            if(data.count > 0){
                var app = [ReviewModel](dictionaryArray: data as! [NSDictionary]);
                completion(true, app)
            }else{
                completion(false, nil)
            }
            
            
            
            
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            completion(false, nil)
            
        })
        
        
    }
    
    
    class func AddReview(id: Int, type: String, postData: [String:Any], completion: @escaping ( _ callback: Bool ) -> Void){
        
        let api_string = (type == "stylist") ? api.setStylistReviews : api.setShopReviews
        
        // Network Request
        NetworkHelper.MakePostRequest("\(api_string)/\(id)", postData: postData, showLoader: true, success: { (successData) -> Void in
            
            completion(true)
            
        },failure: { (error) -> Void in
            
            print(error?.localizedDescription ?? "ERROR");
            let err = error as! NSDictionary;
            UtilityHelper.AlertMessage(err["message"] as! String)
            completion(false)
            
        })
        
        
    }
    
    
    
    
    

    
}
