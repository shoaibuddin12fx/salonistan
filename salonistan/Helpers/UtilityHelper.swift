//
//  UtilityHelper.swift
//  gpsfirst
//
//  Created by Osama Ahmed on 25/09/2018.
//  Copyright © 2018 reaverb. All rights reserved.
//

import Foundation
import UIKit
import LKAlertController
import AVFoundation
import Alamofire
import AlamofireImage

open class UtilityHelper
{
    class func getCurrentDateTime() -> Date{
        
        let currentDate = Date()
        let CurrentTimeZone = NSTimeZone(abbreviation: "GMT")
        let SystemTimeZone = NSTimeZone.system as NSTimeZone
        let currentGMTOffset: Int? = CurrentTimeZone?.secondsFromGMT(for: currentDate)
        let SystemGMTOffset: Int = SystemTimeZone.secondsFromGMT(for: currentDate)
        let interval = TimeInterval((SystemGMTOffset - currentGMTOffset!))
        let TodayDate = Date(timeInterval: interval, since: currentDate)
        print("Current time zone Today Date : \(TodayDate)")
        return TodayDate;
        
    }
    
    class func getCurrentDateTime(date: Date) -> Date{
        
        let currentDate = date;
        let CurrentTimeZone = NSTimeZone(abbreviation: "GMT")
        let SystemTimeZone = NSTimeZone.system as NSTimeZone
        let currentGMTOffset: Int? = CurrentTimeZone?.secondsFromGMT(for: currentDate)
        let SystemGMTOffset: Int = SystemTimeZone.secondsFromGMT(for: currentDate)
        let interval = TimeInterval((SystemGMTOffset - currentGMTOffset!))
        let TodayDate = Date(timeInterval: interval, since: currentDate)
        print("Current time zone Today Date : \(TodayDate)")
        return TodayDate;
        
    }

    
    class func shareYourLink(_ view: UIViewController, link: String? = ""){
        
        let lin = "salonistan.com"
        // set up activity view controller
        let activityViewController = UIActivityViewController(activityItems: [lin], applicationActivities: nil)
//        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = view.view // so that iPads won't crashs
        
        
        // present the view controller
        view.present(activityViewController, animated: true, completion: nil)
    }
    
    class func setImage(_ imgView: UIImageView, _ name: String){
        let url = api.IMAGEURL + name;
        if let curl = URL(string: url){
            imgView.af_setImage(withURL: curl);
        }
    }
    
    
    class func fulldateStringToDate(_ date: String) -> String{
        let s1 = date.components(separatedBy: "T");
        return s1[0];
    }
    
    class func fulldateStringToTime(_ date: String) -> String{
        
        let s1 = date.components(separatedBy: "T")[1];
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss";
        dateFormatter.locale = Locale(identifier: "en_US")
        let d  = dateFormatter.date(from: s1)
        let d2 = dateFormatter.string(from: d!)
        let newFormat = DateFormatter();
        newFormat.dateFormat = "hh:mm a";
        if let d2 = dateFormatter.date(from: d2){
            return newFormat.string(from: d2)
        }else{
            return ""
        }
    }
    
    
    
    
    class func getPlistContent(name: String) -> [[String: Any]]{
        
        var plist: [[String: Any]]!;
        
        if let path = Bundle.main.path(forResource: name, ofType: "plist") {
            
            //If your plist contain root as Array
            if let array = NSArray(contentsOfFile: path) as? [[String: Any]] {
                //print(array);
                plist = array;
            }
            
        }
        
        return plist;
        
    }
    
    class func ShowLoader(_ title:String = "Loading") {

        
        
        
        if ARSLineProgress.shown { return }

        let lview = Bundle.main.loadNibNamed("LView", owner: nil, options: nil)?[0] as! LView;
        let currentWindow: UIWindow = UIApplication.shared.keyWindow!;
        lview.frame = CGRect(x: 0, y: 0, width: currentWindow.frame.width, height: currentWindow.frame.height);
        lview.tag = 9876;
        currentWindow.addSubview(lview);

        ARSLineProgress.showWithPresentCompetionBlock { () -> Void in
            print("Showed with completion block")


        }
        
    }
    
    class func HideLoader() {
        
        if !ARSLineProgress.shown { return }
        
        let currentWindow: UIWindow = UIApplication.shared.keyWindow!;
        for v in currentWindow.subviews{
            if v is LView{
                v.removeFromSuperview();
            }
        }
        
        
        DispatchQueue.main.async {
            ARSLineProgress.hideWithCompletionBlock({ () -> Void in
                print("Hidden with completion block")
            })
        }
        
    }
    
    class func AlertMessage(_ msg: String) {
        Alert(title: nil, message: msg)
            .addAction("Ok")
            .show()
    }
    
    class func AlertMessagewithCallBack(_ msg: String,  success: @escaping () -> Void) {
        
        Alert(title: nil, message: msg).addAction("OK", style: .default) { (_) -> Void in
            success();
            }.show();
        
    }
    
    class func AlertMessageActionSheet(_ title:String,msg: String){
        
        ActionSheet(title: title, message: msg)
            .addAction("Ok")
            .show();
    }
    
    class func hideTopBar(v: UIViewController, _ flag: Bool){
        v.navigationController?.navigationBar.isHidden = flag;
        v.navigationController?.setNavigationBarHidden(flag, animated: false)
    }
    
    class func convertImageToBase64(image: UIImage) -> String {
        let imageData = UIImagePNGRepresentation(image)!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    class func stringFromDPToSetforFormat(_ date: String) -> String{
        let s1 = date.components(separatedBy: " ")[0];
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd";
        dateFormatter.locale = Locale(identifier: "en_US")
        let d  = dateFormatter.date(from: s1)
        let d2 = dateFormatter.string(from: d!)
        let newFormat = DateFormatter();
        newFormat.dateFormat = "mm/dd/yyyy";
        if let d2 = dateFormatter.date(from: d2){
            return newFormat.string(from: d2)
        }else{
            return ""
        }
    }

}
