//
//  LocationHelper.swift
//  gpsfirst
//
//  Created by Osama Ahmed on 18/10/2018.
//  Copyright © 2018 reaverb. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit;

@objc protocol LocationHelperDelegate{
    func returnedLocation(location: CLLocation)
}

class LocationHelper{
    
    // MARK: - Singleton
    static let shared = LocationHelper();
    var locationManager = CLLocationManager();
    weak var delegate: LocationHelperDelegate?
    
    func checkStatus() -> Bool{
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            //
            self.askForPermission();
            return false;
        case .authorizedWhenInUse:
            //
            self.askForPermission();
            return false;
        case .authorizedAlways:
            //
            self.setUpGeoFences();
            return true;
        case .denied:
            //
            self.showSorryAlert();
            return false;
        case .restricted:
            //
            self.showSorryAlert();
            return false;
        }
        
        
    }
    
    private func askForPermission(){
        
        locationManager.requestAlwaysAuthorization();
        locationManager.requestWhenInUseAuthorization();
        
        
    }
    
    private func setUpGeoFences(){
        
    }
    
    private func showSorryAlert(){
        DispatchQueue.main.async {
            UtilityHelper.AlertMessage("Denied Location Service: Some of the features may not work");
        }
        
    }
    
    func getCurrentLocation() -> CLLocation?{
        return locationManager.location ?? nil;
    }
    
    func getCllocation(annotation: MKAnnotation) -> CLLocation {
        return CLLocation(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude);
    }
    
    func getCllocation(location: CLLocationCoordinate2D) -> CLLocation {
        return CLLocation(latitude: location.latitude, longitude: location.longitude);
    }
    
    
    
    func lookUpCurrentLocation(location: CLLocation, completionHandler: @escaping (CLPlacemark?)
        -> Void ) {
        // Use the last reported location.
        
        let lastLocation = location;
        let geocoder = CLGeocoder()
            
            // Look up the location and pass it to the completion handler
        geocoder.reverseGeocodeLocation(lastLocation,
                                            completionHandler: { (placemarks, error) in
                                                if error == nil {
                                                    let firstLocation = placemarks?[0]
                                                    completionHandler(firstLocation)
                                                }
                                                else {
                                                    // An error occurred during geocoding.
                                                    completionHandler(nil)
                                                }
            })
        
    }
    
}
