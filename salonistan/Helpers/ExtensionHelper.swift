//
//  ExtensionHelper.swift
//  gpsfirst
//
//  Created by Osama Ahmed on 25/09/2018.
//  Copyright © 2018 reaverb. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
}

extension Optional where Wrapped == String {
    var nilIfEmpty: String? {
        guard let strongSelf = self else {
            return nil
        }
        return strongSelf.isEmpty ? nil : strongSelf
    }
}

extension NSArray
{
    
    func ToString() ->String
    {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            return jsonString;
            // here "jsonData" is the dictionary encoded in JSON data
        } catch let error as NSError {
            print(error)
        }
        return "";
    }
    
}

extension Int{
    func  toString() -> String{
        return String(describing: self);
    }
    
    static func random(range: Range<Int> ) -> Int
    {
        var offset = 0
        
        if range.lowerBound < 0   // allow negative ranges
        {
            offset = abs(range.lowerBound)
        }
        
        let mini = UInt32(range.lowerBound + offset)
        let maxi = UInt32(range.upperBound   + offset)
        
        return Int(mini + arc4random_uniform(maxi - mini)) - offset
    }
    
}

extension UIView{
    
    func setBackgroundColor(color: String){
        self.backgroundColor = StyleHelper.colorWithHexString(color);
    }
    
    func setBorderBottom(color : String, height: CGFloat){
        let v = self;
        v.layoutIfNeeded();
        let layer = CAShapeLayer()
        layer.path = UIBezierPath(roundedRect: CGRect(x: 0, y: v.frame.height - 1, width: v.frame.width, height: height), cornerRadius: 0).cgPath
        layer.fillColor = StyleHelper.colorWithHexString(color).cgColor
        v.layer.addSublayer(layer)
        v.layer.masksToBounds = true
    }
    
    func removeAllLayers(){
        let v = self;
        
        guard let n = v.layer.sublayers else { return }
        
        for layer in n{
            if(layer is CAShapeLayer){
                layer.removeFromSuperlayer();
            }
            
        }
    }
    
    func snapshot(of rect: CGRect? = nil) -> UIImage? {
        // snapshot entire view
        
        if(rect == nil){
            UIGraphicsBeginImageContextWithOptions(CGSize(width: round(self.bounds.size.width), height: round(self.bounds.size.height)), false, 0.0)
            let contextx: CGContext? = UIGraphicsGetCurrentContext()
            self.layer.render(in: contextx!)
            let capturedScreen: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return capturedScreen!
        }else{
            
            UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0);
            
            
            //UIGraphicsBeginImageContext(self.frame.size)
            drawHierarchy(in: bounds, afterScreenUpdates: true)
            let wholeImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // if no `rect` provided, return image of whole view
            
            guard let image = wholeImage, let rect = rect else { return wholeImage }
            
            // otherwise, grab specified `rect` of image
            
            let scale = image.scale
            let scaledRect = CGRect(x: rect.origin.x * scale, y: rect.origin.y * scale, width: rect.size.width * scale, height: rect.size.height * scale)
            guard let cgImage = image.cgImage?.cropping(to: scaledRect) else { return nil }
            return UIImage(cgImage: cgImage, scale: scale, orientation: .up)
            
        }
        
    }
    
}





