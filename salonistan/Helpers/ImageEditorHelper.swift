//
//  ImageEditorHelper.swift
//  PolaroidPop
//
//  Created by clines227 on 17/07/2017.
//  Copyright © 2017 matech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation


let context = CIContext(options: nil)

class ImageEditor {
    
    class func getImageSizeInMB(_ image: UIImage) -> Double{
        let imgData: NSData = NSData(data: UIImageJPEGRepresentation((image), 1)!)
        let imageSize: Int = imgData.length
        return (Double(imageSize) / 1024.0)
    }
    
    class func transformRotateByDegrees(oldImage: UIImage, deg degrees: CGFloat) -> UIImage {
        
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: oldImage.size.width, height: oldImage.size.height))
        
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat(M_PI / 180))
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, oldImage.scale);
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat(M_PI / 180)))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        
        let rect = CGRect(origin: CGPoint(x: -oldImage.size.width / 2,  y: -oldImage.size.height / 2), size: oldImage.size)
        bitmap.draw(oldImage.cgImage!, in: rect)
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    class func transformFlipHorizontal(oldImage: UIImage) -> UIImage {
        let degrees: CGFloat = 0.0;
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: oldImage.size.width, height: oldImage.size.height))
        
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat(M_PI / 180))
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, oldImage.scale);
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat(M_PI / 180)))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: -1.0, y: -1.0)
        
        let rect = CGRect(origin: CGPoint(x: -oldImage.size.width / 2,  y: -oldImage.size.height / 2), size: oldImage.size)
        bitmap.draw(oldImage.cgImage!, in: rect)
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    class func transformFlipVertical(oldImage: UIImage) -> UIImage {
        let degrees: CGFloat = 0.0;
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: oldImage.size.width, height: oldImage.size.height))
        
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat(M_PI / 180))
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, oldImage.scale);
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat(M_PI / 180)))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: -1.0, y: 1.0);
        
        let rect = CGRect(origin: CGPoint(x: -oldImage.size.width / 2,  y: -oldImage.size.height / 2), size: oldImage.size)
        bitmap.draw(oldImage.cgImage!, in: rect)
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return self.transformFlipHorizontal(oldImage: newImage);
    }
    
    class func transformMirrorHorizontal(view: UIView, oldImage: UIImage, tap: UITapGestureRecognizer) -> UIImage{
        
        // sudo code
        
        /*
         
         find the tap position
         
         if upper area tapped
         crop the upper half area
         transform it and attached to lower half area in the view
         make a snapshot and return
         
         if lower area tapped
         crop the lower half area
         transform it and attached to upper half area in the view
         make a snapshot and return
         
         */
        
        let tempView: UIView = UIView();
        tempView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height);
        
        let fullimageview = UIImageView(image: oldImage);
        fullimageview.frame = tempView.frame;
        tempView.addSubview(fullimageview);
        
        
        let touchPoint = tap.location(in: view);
        let fullHeight = CGFloat(view.frame.height);
        let halfHeight = CGFloat(view.frame.height/2);
        let fullWidth  = CGFloat(view.frame.width);
        
        if(touchPoint.y > halfHeight){
            
            // lower part is touched
            print("lower part", touchPoint.y);
            
            let rect = CGRect(x: 0, y: fullHeight - halfHeight, width: fullWidth, height: halfHeight);
            let bottomHalfImage = transformFlipVertical(oldImage: tempView.snapshot(of: rect)!);
            
            let halfimageview = UIImageView(image: bottomHalfImage);
            let posrect = CGRect(x: 0, y: 0, width: fullWidth, height: halfHeight);
            halfimageview.frame = posrect;
            tempView.addSubview(halfimageview);
            
            return tempView.snapshot()!;
            
        }else{
            // upper part is touched
            print("upper part", touchPoint.y);
            
            let rect = CGRect(x: 0, y: 0, width: fullWidth, height: halfHeight);
            let topHalfImage = transformFlipVertical(oldImage: tempView.snapshot(of: rect)!);
            
            let halfimageview = UIImageView(image: topHalfImage);
            let posrect = CGRect(x: 0, y: fullHeight - halfHeight, width: fullWidth, height: halfHeight);
            halfimageview.frame = posrect;
            tempView.addSubview(halfimageview);
            
            return tempView.snapshot()!;
        }
        
    }
    
    class func transformMirrorVertical(view: UIView, oldImage: UIImage, tap: UITapGestureRecognizer) -> UIImage{
        
        // sudo code
        
        /*
         
         find the tap position
         
         if upper area tapped
         crop the upper half area
         transform it and attached to lower half area in the view
         make a snapshot and return
         
         if lower area tapped
         crop the lower half area
         transform it and attached to upper half area in the view
         make a snapshot and return
         
         */
        
        let tempView: UIView = UIView();
        tempView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height);
        
        let fullimageview = UIImageView(image: oldImage);
        fullimageview.frame = tempView.frame;
        tempView.addSubview(fullimageview);
        
        
        let touchPoint = tap.location(in: view);
        let fullWidth = CGFloat(view.frame.width);
        let halfWidth = CGFloat(view.frame.width/2);
        let fullHeight  = CGFloat(view.frame.height);
        
        if(touchPoint.x > halfWidth){
            
            // right part is touched
            print("right part", touchPoint.x);
            
            let rect = CGRect(x: fullWidth - halfWidth, y: 0, width: halfWidth, height: fullHeight);
            let rightHalfImage = transformFlipHorizontal(oldImage: tempView.snapshot(of: rect)!);
            
            let halfimageview = UIImageView(image: rightHalfImage);
            let posrect = CGRect(x: 0, y: 0, width: halfWidth, height: fullHeight);
            halfimageview.frame = posrect;
            tempView.addSubview(halfimageview);
            
            return tempView.snapshot()!;
            
        }else{
            // upper part is touched
            print("left part", touchPoint.y);
            
            let rect = CGRect(x: 0, y: 0, width: halfWidth, height: fullHeight);
            let leftHalfImage = transformFlipHorizontal(oldImage: tempView.snapshot(of: rect)!);
            
            let halfimageview = UIImageView(image: leftHalfImage);
            let posrect = CGRect(x: fullWidth - halfWidth, y: 0, width: halfWidth, height: fullHeight);
            halfimageview.frame = posrect;
            tempView.addSubview(halfimageview);
            
            return tempView.snapshot()!;
        }
        
    }
    
    class func transformMirror45a(view: UIView, oldImage: UIImage, tap: UITapGestureRecognizer) -> UIImage{
        
        // sudo code
        
        /*
         take a snapshot of full image
         add it to full UIView
         create a UIImageView triangular and add the image to it
         transform the UIImageView / flip the UIImageView
         take the snapshot of whole UIView
         
         */
        
        let tempView: UIView = UIView();
        tempView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height);
        
        let fullimageview = UIImageView(image: oldImage);
        fullimageview.frame = tempView.frame;
        tempView.addSubview(fullimageview);
        
        
        
        
        let touchPoint = tap.location(in: view);
        let fullWidth = CGFloat(view.frame.width);
        let fullHeight  = CGFloat(view.frame.height);
        let halfWidth = CGFloat(view.frame.width/2);
        let halfHeight = CGFloat(view.frame.height/2);
        
        
        
        if(touchPoint.x > halfWidth && touchPoint.y > halfHeight){
            print("Mirror 45a bottom right");
            
            let flipImage = self.transformFlipVertical(oldImage: oldImage);
            
            let halfimageview = UIImageView(image: flipImage);
            halfimageview.frame = tempView.frame;
            tempView.addSubview(fullimageview);
            
            // Build a triangular path
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: fullWidth, y: 0))
            path.addLine(to: CGPoint(x: 0, y: fullHeight));
            path.addLine(to: CGPoint(x: 0, y: 0))
            // Create a CAShapeLayer with this triangular path
            // Same size as the original imageView
            let mask = CAShapeLayer()
            mask.frame = halfimageview.bounds
            mask.path = path.cgPath
            // Mask the imageView's layer with this shape
            halfimageview.layer.mask = mask
            
            tempView.addSubview(halfimageview);
            return tempView.snapshot()!;
            
            
            
            
            
        } else if(touchPoint.x < halfWidth && touchPoint.y < halfHeight){
            print("Mirror 45a top left");
            
            let flipImage = self.transformFlipVertical(oldImage: oldImage);
            
            let halfimageview = UIImageView(image: flipImage);
            halfimageview.frame = tempView.frame;
            tempView.addSubview(fullimageview);
            
            // Build a triangular path
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x: fullWidth, y: 0))
            path.addLine(to: CGPoint(x: fullWidth, y: fullHeight))
            path.addLine(to: CGPoint(x: 0, y: fullHeight));
            path.addLine(to: CGPoint(x: fullWidth, y: 0))
            // Create a CAShapeLayer with this triangular path
            // Same size as the original imageView
            let mask = CAShapeLayer()
            mask.frame = halfimageview.bounds
            mask.path = path.cgPath
            // Mask the imageView's layer with this shape
            halfimageview.layer.mask = mask
            
            tempView.addSubview(halfimageview);
            return tempView.snapshot()!;
            
        }else{
            
            return oldImage;
            
        }
        
    }
    
    class func transformMirror45b(view: UIView, oldImage: UIImage, tap: UITapGestureRecognizer) -> UIImage{
        
        // sudo code
        
        /*
         take a snapshot of full image
         add it to full UIView
         create a UIImageView triangular and add the image to it
         transform the UIImageView / flip the UIImageView
         take the snapshot of whole UIView
         
         */
        
        let tempView: UIView = UIView();
        tempView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height);
        
        let fullimageview = UIImageView(image: oldImage);
        fullimageview.frame = tempView.frame;
        tempView.addSubview(fullimageview);
        
        
        
        
        let touchPoint = tap.location(in: view);
        let fullWidth = CGFloat(view.frame.width);
        let fullHeight  = CGFloat(view.frame.height);
        let halfWidth = CGFloat(view.frame.width/2);
        let halfHeight = CGFloat(view.frame.height/2);
        
        
        
        if(touchPoint.x < halfWidth && touchPoint.y > halfHeight){
            print("Mirror 45a top right");
            
            let flipImage = self.transformFlipVertical(oldImage: oldImage);
            
            let halfimageview = UIImageView(image: flipImage);
            halfimageview.frame = tempView.frame;
            tempView.addSubview(fullimageview);
            
            // Build a triangular path
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: fullWidth, y: 0))
            path.addLine(to: CGPoint(x: fullWidth, y: fullHeight));
            path.addLine(to: CGPoint(x: 0, y: 0))
            // Create a CAShapeLayer with this triangular path
            // Same size as the original imageView
            let mask = CAShapeLayer()
            mask.frame = halfimageview.bounds
            mask.path = path.cgPath
            // Mask the imageView's layer with this shape
            halfimageview.layer.mask = mask
            
            tempView.addSubview(halfimageview);
            return tempView.snapshot()!;
            
            
            
            
            
        } else if(touchPoint.x > halfWidth && touchPoint.y < halfHeight){
            print("Mirror 45a bottom left");
            
            let flipImage = self.transformFlipVertical(oldImage: oldImage);
            
            let halfimageview = UIImageView(image: flipImage);
            halfimageview.frame = tempView.frame;
            tempView.addSubview(fullimageview);
            
            // Build a triangular path
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 0, y: 0))
            path.addLine(to: CGPoint(x: fullWidth, y: fullHeight))
            path.addLine(to: CGPoint(x: 0, y: fullHeight));
            path.addLine(to: CGPoint(x: 0, y: 0))
            // Create a CAShapeLayer with this triangular path
            // Same size as the original imageView
            let mask = CAShapeLayer()
            mask.frame = halfimageview.bounds
            mask.path = path.cgPath
            // Mask the imageView's layer with this shape
            halfimageview.layer.mask = mask
            
            tempView.addSubview(halfimageview);
            return tempView.snapshot()!;
            
        }else{
            
            return oldImage;
            
        }
        
    }
    
    class func returnImageOfFixSize(oldImage: UIImage, size: CGSize) -> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 1.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale);
        oldImage.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
        
    }
    
    class func resizeImageOfRatio(image: UIImage, ratio: CGFloat) -> UIImage {
        let resizedSize = CGSize(width: Int(image.size.width * ratio), height: Int(image.size.height * ratio))
        UIGraphicsBeginImageContext(resizedSize)
        image.draw(in: CGRect(x: 0, y: 0, width: resizedSize.width, height: resizedSize.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage!
    }
    
    class func createHueImage(hue: CGFloat, image: UIImage) -> UIImage {
        // Create a place to render the filtered image
        // let context = CIContext(options: nil)
        
        // Create an image to filter
        let inputImage = CIImage(image: image);
        
        // Create a random color to pass to a filter
        let randomColor = [kCIInputAngleKey: (Double(hue) / 100)]
        
        // Apply a filter to the image
        let filteredImage = inputImage!.applyingFilter("CIHueAdjust", parameters: randomColor)
        
        // Render the filtered image
        let renderedImage = context.createCGImage(filteredImage, from: filteredImage.extent)
        
        // Return a UIImage
        return UIImage(cgImage: renderedImage!)
    }
    
    
    class func createFilteredImage(filterName: String, image: UIImage) -> UIImage {
        
        //let context = CIContext(options: nil)
        
        // 1 - create source image
        let sourceImage = CIImage(image: image)
        
        // 2 - create filter using name
        let filter = CIFilter(name: filterName)
        filter?.setDefaults()
        
        // 3 - set source image
        filter?.setValue(sourceImage, forKey: kCIInputImageKey)
        
        // 4 - output filtered image as cgImage with dimension.
        let outputCGImage = context.createCGImage((filter?.outputImage!)!, from: (filter?.outputImage!.extent)!)
        
        // 5 - convert filtered CGImage to UIImage
        let filteredImage = UIImage(cgImage: outputCGImage!)
        
        return filteredImage
    }
    
    class func createBlurFilteredImage(filterName: String, radius: Double, image: UIImage) -> UIImage {
        
        //let context = CIContext(options: nil)
        
        // Create an image to filter
        let inputImage = CIImage(image: image);
        //let desparityImage = CIImage(image: image, options: [kCIImageAuxiliaryDisparity : true]);
        
        // Create a random color to pass to a filter
        //let randomBlur = [kCIInputRadiusKey: (Double(image.size.width) / 4)]
        let randomBlur = [kCIInputRadiusKey: radius]
        
        // Apply a filter to the image
        let filteredImage = inputImage!.applyingFilter(filterName, parameters: randomBlur)
        
        // Render the filtered image
        let renderedImage = context.createCGImage(filteredImage, from: filteredImage.extent)
        
        // Return a UIImage
        return UIImage(cgImage: renderedImage!)
        
    }
    
    
    class func applyBlurEffect(image: UIImage) -> UIImage{
        let imageToBlur = CIImage(image: image)
        let blurfilter = CIFilter(name: "CIGaussianBlur")
        blurfilter?.setValue(imageToBlur, forKey: "inputImage")
        blurfilter?.setValue(9, forKey: "inputRadius")
        let resultImage = blurfilter?.value(forKey: "outputImage") as! CIImage
        let blurredImage = UIImage(ciImage: resultImage)
        return blurredImage
    }
    
    
    class func imageScaledToFit(oldImage: UIImage, size: CGSize) -> UIImage {
        let scaledRect = AVMakeRect(aspectRatio: oldImage.size, insideRect: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        oldImage.draw(in: scaledRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()// as? UIImage
        UIGraphicsEndImageContext()
        return scaledImage ?? UIImage()
    }
    
    class func imageScaledToFitWithScale(oldImage: UIImage, size: CGSize ,scale : CGFloat) -> UIImage {
        
        let ActualSize =  CGSize(width: oldImage.size.width  , height: oldImage.size.height)
        let scaledRect = AVMakeRect(aspectRatio: ActualSize, insideRect: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        oldImage.draw(in: scaledRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()// as? UIImage
        UIGraphicsEndImageContext()
        return scaledImage ?? UIImage()
    }
    
    
    class func returnHexColorFromValue(value: Int) -> String{
        // RRGGBB hex colors in the same order as the image
        let colorArray = [ "#000000", "#262626", "#4d4d4d", "#666666", "#808080", "#990000", "#cc0000", "#fe0000", "#ff5757", "#ffabab", "#ffabab", "#ffa757", "#ff7900", "#cc6100", "#994900", "#996f00", "#cc9400", "#ffb900", "#ffd157", "#ffe8ab", "#fff4ab", "#ffe957", "#ffde00", "#ccb200", "#998500", "#979900", "#cacc00", "#fcff00", "#fdff57", "#feffab", "#f0ffab", "#e1ff57", "#d2ff00", "#a8cc00", "#7e9900", "#038001", "#04a101", "#05c001", "#44bf41", "#81bf80", "#81c0b8", "#41c0af", "#00c0a7", "#00a18c", "#00806f", "#040099", "#0500cc", "#0600ff", "#5b57ff", "#adabff", "#d8abff", "#b157ff", "#6700bf", "#5700a1", "#450080", "#630080", "#7d00a1", "#9500c0", "#a341bf", "#b180bf", "#bf80b2", "#bf41a6", "#bf0199", "#a10181", "#800166", "#999999", "#b3b3b3", "#cccccc", "#e6e6e6", "#ffffff"];
        
        return colorArray[value];
        
    }
    
    class func changeMultiplier(_ constraint: NSLayoutConstraint, multiplier: CGFloat) -> NSLayoutConstraint {
        let newConstraint = NSLayoutConstraint(
            item: constraint.firstItem,
            attribute: constraint.firstAttribute,
            relatedBy: constraint.relation,
            toItem: constraint.secondItem,
            attribute: constraint.secondAttribute,
            multiplier: multiplier,
            constant: constraint.constant)
        
        newConstraint.priority = constraint.priority
        
        NSLayoutConstraint.deactivate([constraint])
        NSLayoutConstraint.activate([newConstraint])
        
        return newConstraint
    }
    
    
    
    
}
