//
//  DesignHelper.swift
//  salonistan
//
//  Created by Osama Ahmed on 09/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    
    func ThemeType1(){
        let v = self;
        v.tintColor = UIColor.white;
        v.setTitleColor(UIColor.white, for: .normal);
        v.setBackgroundColor(color: Colors.red);
        v.roundBorder(color: Colors.red, width: 1);
    }
    
    func ThemeType1Transparent(){
        let v = self;
        v.tintColor = UIColor.white;
        v.setTitleColor(UIColor.white, for: .normal);
        v.backgroundColor = UIColor.clear
        v.roundBorder(color: Colors.red, width: 0);
    }
    
    func ThemeType1White(){
        let v = self;
        let red = StyleHelper.colorWithHexString(Colors.red)
        v.tintColor = red;
        v.setTitleColor(red, for: .normal);
        v.backgroundColor = UIColor.white
        v.roundBorder(color: Colors.white, width: 1);
    }
    
}

class DesignHelper{
    
    
    
}
