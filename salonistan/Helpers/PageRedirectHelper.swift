//
//  PageRedirectHelper.swift
//  gpsfirst
//
//  Created by Osama Ahmed on 24/09/2018.
//  Copyright © 2018 reaverb. All rights reserved.
//

import Foundation
import UIKit
import SwiftIconFont
import CoreLocation


class PageRedirect {
    
    class func SplashPage(viewController: UIViewController){
        
        UtilityHelper.ShowLoader();
        
        var splashVC: SplashVC!;
        
        for item in (viewController.navigationController?.viewControllers)! {
            
            if ( item is SplashVC ){
                splashVC = item as! SplashVC;
                break
            }
            
        }
        
        UtilityHelper.HideLoader();
        guard let s = splashVC else {
            return
        }
        
        viewController.navigationController?.popToViewController(s, animated: true);
        
    }
    
    class func LoginPage(viewController: UIViewController){
        
        UtilityHelper.ShowLoader();
        
        var loginVC: LoginVC!;
        
        for item in (viewController.navigationController?.viewControllers)! {
            
            if ( item is LoginVC ){
                loginVC = item as! LoginVC;
                break
            }
            
        }
        
        UtilityHelper.HideLoader();
        if let i = loginVC {
            viewController.navigationController?.popToViewController(i, animated: true);
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
            let destination = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC;
            viewController.navigationController?.pushViewController(destination, animated: true);
        }
        
        
        
        
        
    }
    
    class func SignupPage(viewController: UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func EditProfilePage(viewController: UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    
    class func ForgetPage(viewController: UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func DashboardPage(viewController: UIViewController){
        
        UtilityHelper.ShowLoader();
        
        var _vc: DashboardVC!;
        
        for item in (viewController.navigationController?.viewControllers)! {
            
            if ( item is DashboardVC ){
                _vc = item as! DashboardVC;
                break
            }
            
        }
        
        UtilityHelper.HideLoader();
        if let i = _vc {
            viewController.navigationController?.popToViewController(i, animated: true);
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
            let destination = storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC;
            viewController.navigationController?.pushViewController(destination, animated: true);
        }
        
        
        
    }
    
    class func PreLoginPage(viewController: UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "PreLoginVC") as! PreLoginVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func PreSignupPage(viewController: UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "PreSignupVC") as! PreSignupVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func SearchPage(viewController: UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func ServicesPage(viewController: UIViewController, cat: CategoryModel){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "ServiceVC") as! ServiceVC;
        destination.category = cat;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func SalonsOfService(viewController: UIViewController, service: ServiceModel){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "SalonsVC") as! SalonsVC;
        destination.service = service;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func SubSalon(viewController: UIViewController, salon: SalonModel?, stylist: StylistDetailModel?, isStylist: Bool ){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "SubSalonVC") as! SubSalonVC;
        destination.singleSalon = salon;
        destination.singleStylist = stylist;
        destination.isStylist = isStylist;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func InvitePage(viewController: UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "InviteVC") as! InviteVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func SettingsPage(viewController: UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func DateTimeSelectorPage(viewController: SubSalonVC, booking: [AssociatedService] ){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "DateTimeSelectorVC") as! DateTimeSelectorVC;
        destination.subSalonVC = viewController
        destination.bookingArray = booking;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func SelectStylistPage(viewController: UIViewController, salonVC: SubSalonVC, booking: [AssociatedService], date: String, time: String ){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "SelectStylistVC") as! SelectStylistVC;
        destination.subSalonVC = salonVC;
        destination.bookingArray = booking;
        destination.final_date = date;
        destination.final_time = time;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func FinalBookingPage(viewController: UIViewController, salonVC: SubSalonVC, booking: [AssociatedService], date: String, time: String, stylist: AssociatedStylist? ){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "FinalBookingVC") as! FinalBookingVC;
        destination.subSalonVC = salonVC;
        destination.bookingArray = booking;
        destination.final_date = date;
        destination.final_time = time;
        destination.selectedStylist = stylist;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func AppointmentsPage(viewController: UIViewController){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "AppointmentsTabVC") as! AppointmentsTabVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func AppointmentDetailPage(viewController: UIViewController, ap_id: Int, ap_type: String ){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "AppointmentDetailVC") as! AppointmentDetailVC;
       
        destination.ap_id = ap_id;
        destination.ap_type = ap_type;
        
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func ReviewPage(viewController: UIViewController, id: Int, type: String ){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC;
        
        destination.reviewId = id;
        destination.reviewType = type;
        
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    class func ChangePasswordPage(viewController: UIViewController ){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main);
        let destination = storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC;
        viewController.navigationController?.pushViewController(destination, animated: true);
        
    }
    
    
    
    
    
    
    

    

    
    
    
    
    
    
    
}
