//
//  NetworkHelper.swift
//  gpsfirst
//
//  Created by Osama Ahmed on 25/09/2018.
//  Copyright © 2018 reaverb. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import EVReflection

class NetworkHelper {
    
    class func maximumValue<T: Comparable>(first: T, second: T) -> T
    {
        if (first >= second)
        {
            return first
        }
        
        return second
    }
    
    
    class func MakeDeleteRequestWithString(
        _ Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        alertMessage: Bool = false,
        timeOut : Double = 0,
        success: ((_ successData : [String: Any]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        
        headers : [String: String]? = nil){
        
        MakeRequestWithStringData(HTTPMethod.delete, Url: Url, postData: postData,alertMessage: alertMessage, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    class func MakeGetRequestWithStringData(
        _ Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        alertMessage: Bool = false,
        timeOut : Double = 0,
        success: ((_ successData : [String: Any]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        
        headers : [String: String]? = nil){
        
        MakeRequestWithStringData(HTTPMethod.get, Url: Url, postData: postData,alertMessage: alertMessage, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    class func MakePostRequestWithStringData(
        _ Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        alertMessage: Bool = false,
        timeOut : Double = 0,
        success: ((_ successData : [String: Any]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        
        headers : [String: String]? = nil){
        
        MakeRequestWithStringData(HTTPMethod.post, Url: Url, postData: postData,alertMessage: alertMessage, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    
    
    class func MakePostRequest(
        _ Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [String:Any]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
        
        MakeRequest(HTTPMethod.post, Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    
    class func MakeDeleteRequest(
        _ Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [String:Any]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
        
        MakeRequest(HTTPMethod.delete, Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    
    class func MakeGetRequest(
        _ Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [String:Any]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
        
        MakeRequest(HTTPMethod.get, Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    
    
    
    class func MakePutRequest(
        _ Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [String:Any]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
        
        MakeRequest(HTTPMethod.put, Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    
    class func MakeRequest(
        _ method: HTTPMethod,
        Url: String,
        postData: [String: Any]? = nil ,
        params:Parameters? = [:],
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [String: Any]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil,
        retry:Bool = false
        ){
        
        if(showLoader){
            UtilityHelper.ShowLoader();
        }
        
        let Url = api.SERVICEURL + Url;
        
        
        var AllHeaders : [String: String] = [String: String]();
        if(headers != nil){
            AllHeaders = headers!;
        }
        
        let user = UserDataHelper.returnUser()
        if(user != nil){
            //AllHeaders["Authorization"] = user?.token;
        }
        
        
        print(Url);
        
        Alamofire.request(Url, method: method, parameters: postData, encoding: JSONEncoding.default, headers: AllHeaders)
            
            .responseJSON { (response) in
                //
                switch response.result {
                case .success(let JSON):
                    
                    print(JSON);
                    // let res =  response.response!;
                    guard let data = JSON as? [String: Any] else {
                            print("Failed to parse JSON")
                            return
                    }
                    
                    let resObj = ResData(dictionary: data as NSDictionary);
                    print(resObj);
                    
                    if(resObj.error == 0){
                        
                        success?(resObj.data as! [String : Any]);
                        if(showLoader){
                            UtilityHelper.HideLoader();
                        }
                        
                    }
                    else{
                        
                        if(showLoader){
                            UtilityHelper.HideLoader();
                            // UtilityHelper.AlertMessage(resObj.message);
                        }
                        
                        failure?(JSON as AnyObject);
                        
                    }
                    
                    
                case .failure(let error):
                    
                    print("RESPONSE DATA : ERROR: " + error.localizedDescription)
                    if(showLoader){
                        UtilityHelper.HideLoader();
                    }
                    failure?(error as AnyObject);
                    
                }
        }
        
    }
    
    
    
    class func MakeRequestWithStringData(
        _ method: HTTPMethod,
        Url: String,
        postData: [String: Any]? = nil ,
        alertMessage: Bool = false,
        params:Parameters? = [:],
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [String: Any]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil,
        retry:Bool = false
        
        ){
        
        if(showLoader){
            UtilityHelper.ShowLoader();
        }
        
        let Url = api.SERVICEURL + Url;
        
        
        var AllHeaders : [String: String] = [String: String]();
        if(headers != nil){
            AllHeaders = headers!;
        }
        
        let user = UserDataHelper.returnUser()
        if(user != nil){
            //AllHeaders["Authorization"] = user?.token;
        }
        
        
        
        Alamofire.request(Url, method: method, parameters: postData, encoding: JSONEncoding.default, headers: AllHeaders)
            
            .responseJSON { (response) in
                //
                switch response.result {
                case .success(let JSON):
                    
                    print(JSON);
                    // let res =  response.response!;
                    guard let data = JSON as? [String: Any] else {
                        print("Failed to parse JSON")
                        return
                    }
                    print(data)
                 success?(data);
                    if(showLoader){
                        UtilityHelper.HideLoader();
                    }
                    
                case .failure(let error):
                    
                    print("RESPONSE DATA : ERROR: " + error.localizedDescription)
                    if(showLoader){
                        UtilityHelper.HideLoader();
                    }
                    failure?(error as AnyObject);
                    
                }
        }
        
    }
    
    
    
    
    
    
    class func MakePostRequestForArray(
        _ Url: String,
        postData: [String:Any]? = nil ,
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [[String: Any]]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
        
        MakeRequestForArray(HTTPMethod.post, Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    
    class func MakeGetRequestForArray(
        _ Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [[String: Any]]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
        
        MakeRequestForArray(HTTPMethod.get, Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    
    
    class func MakePutRequestForArray(
        _ Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [[String: Any]]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil){
        
        MakeRequestForArray(HTTPMethod.put, Url: Url, postData: postData, showLoader: showLoader, timeOut: timeOut, success: success, failure: failure, headers: headers)
        
    }
    
    class func MakeRequestForArray(
        _ method: HTTPMethod,
        Url: String,
        postData: [String: Any]? = nil ,
        showLoader : Bool = true,
        timeOut : Double = 0,
        success: ((_ successData : [[String: Any]]?) -> Void)!,
        failure: ((_ error : AnyObject?) -> Void)!,
        headers : [String: String]? = nil,
        retry:Bool = false)
    {
        
        var timeOut = timeOut
        
        if(timeOut == 0){
            timeOut  = settings.timeOut;
        }
        
        if(showLoader){
            UtilityHelper.ShowLoader();
        }
        var AllHeaders : [String: String] = [String: String]();
        
        if(headers != nil){
            AllHeaders = headers!;
        }
        
        let Url = api.SERVICEURL + Url;
        
        
        
        print("REQUEST TIMEOUT TIME : \(timeOut)")
        print(Url);
        Alamofire.request(Url, method: method, parameters: postData, encoding: JSONEncoding.default)
            //Alamofire.request(req as! URLRequestConvertible)
            .responseJSON { (response) in
                
                switch response.result {
                case .success(let JSON):
                    
                    print(JSON);
                    // let res =  response.response!;
                    guard let data = JSON as? [String: Any] else {
                        print("Failed to parse JSON")
                        return
                    }
                    
                    let resObj = ResArrayData(dictionary: data as! NSDictionary)
                    print(resObj);
                    
                    if(resObj.error == 0){
                    
                        
                        success?(resObj.data as! [[String : Any]]);
                        
                        
                        if(showLoader){
                            UtilityHelper.HideLoader();
                        }
                        
                    }
                    else{
                        
                        if(showLoader){
                            UtilityHelper.HideLoader();
                            UtilityHelper.AlertMessage(resObj.message);
                        }
                        
                        failure?(JSON as AnyObject);
                        
                    }
                    
                case .failure(let error):
                    
                    print("RESPONSE DATA : ERROR: " + error.localizedDescription)
                    if(showLoader){
                        UtilityHelper.HideLoader();
                    }
                    failure?(error as AnyObject);
                    
                }
                
                
        }
        
    }
    
    
    
}
