//
//  StyleExtensions.swift
//  gpsfirst
//
//  Created by Osama Ahmed on 17/07/2018.
//  Copyright © 2018 reaverb. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    func roundBorder(color : String, width: CGFloat){
        let v = self;
        v.layoutIfNeeded();
        v.layer.cornerRadius = v.frame.size.height/2;
        v.clipsToBounds = true;
        v.layer.borderColor = StyleHelper.colorWithHexString(color).cgColor;
        v.layer.borderWidth = width;
    }
    
    func setBorder(color: String, radius: Int, width: Int){
        self.layoutIfNeeded();
        self.layer.cornerRadius = CGFloat(radius);
        self.clipsToBounds = true;
        self.layer.borderColor = StyleHelper.colorWithHexString(color).cgColor;
        self.layer.borderWidth = CGFloat(width);
    }
    
    func gradient(_ FirstColor: String, _ SecondColor: String){
        let v = self;
        let gradient = CAGradientLayer()
        gradient.frame = v.bounds
        gradient.startPoint = CGPoint.zero;
        gradient.endPoint = CGPoint(x: 1, y: 1);
        gradient.colors = [StyleHelper.colorWithHexString(FirstColor).cgColor, StyleHelper.colorWithHexString(SecondColor).cgColor];
        v.layer.insertSublayer(gradient, at: 0)
    }
    
}
