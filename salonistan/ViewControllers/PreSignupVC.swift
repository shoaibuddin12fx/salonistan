//
//  PreSignupVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 07/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class PreSignupVC: UIViewController {
    //
    
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet var videoView: VideoView!
    @IBOutlet var btnLogin: UIButton!;
    @IBOutlet var btnSignup: UIButton!;
    @IBOutlet var btnSkip: UIButton!;
    
    
    override func viewDidLoad() {
        //
        loadVIsuals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //
        let _ : Void = {
            // UtilityHelper.hideTopBar(v: self, true);
            guard let path = Bundle.main.path(forResource: "video_splash", ofType:"3gp") else {
                debugPrint("video_splash.3gp not found")
                return
            }
            videoView.configure(url: path);
            videoView.isLoop = true;
            videoView.play();
            imgMain.isHidden = true;
            
        }();
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // UtilityHelper.hideTopBar(v: self, false)
    }
    
    func loadVIsuals(){
        btnLogin.ThemeType1();
        btnSkip.ThemeType1();
        btnSignup.ThemeType1Transparent();
    }
    
    @IBAction func login(_ sender: UIButton){
        SettingsDataHelper.skipTutorial(true);
        PageRedirect.LoginPage(viewController: self);
    }
    
    @IBAction func signup(_ sender: UIButton){
        SettingsDataHelper.skipTutorial(true);
        PageRedirect.SignupPage(viewController: self);
    }
    
    @IBAction func skip(_ sender: UIButton){
        SettingsDataHelper.skipTutorial(true);
        PageRedirect.DashboardPage(viewController: self)
    }
    
    
    
    
    
}
