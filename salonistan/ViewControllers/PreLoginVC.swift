//
//  PreLoginVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 24/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class PreLoginVC: UIViewController {
    //
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var btnLogin: UIButton!;
    @IBOutlet var btnSignup: UIButton!;
    
    var testimonials: [[String:Any]] = [[String:Any]]()
    
    override func viewDidLoad() {
        //
        loadVIsuals();
        initCollection();
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, true)
//        }();
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, false)
//    }
    
    func loadVIsuals(){
        btnLogin.ThemeType1();
        btnSignup.ThemeType1White();
    }
    
    @IBAction func login(_ sender: UIButton){
        PageRedirect.LoginPage(viewController: self);
    }
    
    @IBAction func signup(_ sender: UIButton){
        PageRedirect.SignupPage(viewController: self);
    }
    
    
    
    
    
}

extension PreLoginVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func initCollection(){
        
        let nib = UINib(nibName: "TestimonialCVC", bundle: nil);
        collectionView.register(nib, forCellWithReuseIdentifier: "TestimonialCVC")
        collectionView.dataSource = self;
        collectionView.delegate = self;
        collectionView.allowsMultipleSelection = false;
        let data = UtilityHelper.getPlistContent(name: "PreLoginSlider");
        self.testimonials.append(contentsOf: data);
        pageControl.numberOfPages = self.testimonials.count
        collectionView.reloadData();
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //
        let width = collectionView.frame.width;
        let height = collectionView.frame.height;
        return CGSize(width: width, height: height);
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return testimonials.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestimonialCVC", for: indexPath) as! TestimonialCVC
        
        cell.setData(testimonials[indexPath.row])
        
        return cell
        
    }
    
}

