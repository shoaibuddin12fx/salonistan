//
//  ReviewVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 17/02/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class ReviewVC: BaseVC, AddReviewSubViewDelegate {
    //
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnAddReview: UIButton!
    @IBOutlet weak var starReview: CosmosView!
    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var lblAvgCount: UILabel!
    var reviewId: Int = 7;
    var reviewType: String = "salon";
    
    var addReviewSubView: AddReviewSubView!;
    var reviews: [ReviewModel] = [ReviewModel]();
    var avgReview: CGFloat = 0.0;
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Reviews"
        btnAddReview.ThemeType1();
        initTable();
        loadData();
    }
    
    func loadData(){
        ApiHelper.GetReviews(id: self.reviewId, type: reviewType) { (flag, revs) in
            //
            if flag, let _revs = revs{
                    self.reviews = _revs;
                self.lblReviewCount.text = "\(_revs.count) Reviews";
                
                var _temp_avg: CGFloat = 0.0;
                
                for each in _revs{
                    _temp_avg = _temp_avg + CGFloat(truncating: each.rating)
                }
                
                self.avgReview = ( _temp_avg / CGFloat(_revs.count) );
                self.lblAvgCount.text = String(format: "%.1f", self.avgReview);
                self.starReview.rating = Double(self.avgReview)
                
                
                
                
                
                
                    self.tableView.reloadData();
                
            }
        }
    }
    
    @IBAction func addReview(){
        
        if(!UserDataHelper.checkIfUserExist()){
            UtilityHelper.AlertMessage("You must login to add a review")
        }else{
            
            if(addReviewSubView == nil){
                addReviewSubView =  Bundle.main.loadNibNamed("AddReviewSubView", owner: self, options: nil)?[0] as? AddReviewSubView;
                let h = screenSize.size.height;
                let w = screenSize.size.width;
                addReviewSubView.frame = CGRect(x: 0, y: 0, width: w, height: h);
                addReviewSubView.tag = 90;
                addReviewSubView.delegate = self;
                _window.addSubview(addReviewSubView);
                
            }else{
                //unloadMultiPrintView();
            }
            
        }
        
        
        
    }
    
    // delegate functrion
    func submitRefreshView(view: AddReviewSubView, doRefresh: Bool) {
        //
        view.removeFromSuperview();
        addReviewSubView = nil;
        
        if(doRefresh){
            loadData();
        }
        
        
    }
    
    // delegate function
    func submitReview(view: AddReviewSubView, post: [String : Any]) {
        //
        view.removeFromSuperview();
        addReviewSubView = nil;
        
        ApiHelper.AddReview(id: reviewId, type: self.reviewType, postData: post) { (flag) in
            //
            if(flag){
                self.loadData();
            }
        }
        
        
        
        
    }
    
    
}

extension ReviewVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "ReviewTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "ReviewTVC")
        
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return 80.0;
        
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return reviews.count;
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTVC") as! ReviewTVC 
        
        cell.setData(c: self.reviews[indexPath.row])
        
        return cell
       
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
    }
    
    
    
    
}


























