//
//  PreviousAppointmentVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 19/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class PreviousAppointmentVC: UIViewController {
    //
    @IBOutlet weak var tableView: UITableView!
    var appointments: [AppointmentModel] = [AppointmentModel]();
    
    
    override func viewDidLoad() {
        //
        initTable();
        loadData();
        
    }
    
    func loadData(){
        ApiHelper.GetPreviousAppointments { (flag, model) in
            //
            if flag , let m = model{
                self.appointments = m;
                self.tableView.reloadData();
            }
            
        }
    }
}

extension PreviousAppointmentVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "CPAppTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "CPAppTVC")
        
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return UITableViewAutomaticDimension
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return self.appointments.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CPAppTVC") as! CPAppTVC
        cell.setData(self.appointments[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
    
}

