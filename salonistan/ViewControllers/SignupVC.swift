//
//  SignupVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 07/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import ActionSheetPicker_3_0

class SignupVC: BaseVC {
    //
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var tbxFirstName: UITextField!
    @IBOutlet weak var tbxLastName: UITextField!
    @IBOutlet weak var tbxUserName: UITextField!
    @IBOutlet weak var tbxPassword: UITextField!
    @IBOutlet weak var tbxGender: UITextField!
    @IBOutlet weak var imgCheckbox: UIImageView!
    var specialOffer: Bool = false;
    var genderSelectionIndex = 0
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Signup"
        
        loadVisuals();
        tbxGender.text = "Male";
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, true)
//        }();
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, false)
//    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true);
    }
    
    
    @IBAction func checkOffers(_ sender: UIButton){
        
        specialOffer = !specialOffer;
        var icon_name = "check";
        if( specialOffer ){
            icon_name = "check";
        }else{
            icon_name = "times";
        }
        
        StyleHelper.setFontImageVisualsFontAwesome(imgCheckbox, name: icon_name, color: Colors.lightgray);
        
        
    }
    
    @IBAction func signup(_ sender: Any){
        
        // Initializing Items
        let UserName: String = tbxUserName.text!  as String;
        let Password: String = tbxPassword.text! as String;
        let firstname: String = tbxFirstName.text! as String;
        let lastname: String = tbxLastName.text! as String;
        let gender: String = tbxGender.text! as String;
        
        //doing Validations
        if(!Validate.Required(text: UserName, Label: "email or phone")){return;}
        if(!Validate.Required(text: Password, Label: "password")){return;}
        if(!Validate.Required(text: firstname, Label: "first name")){return;}
        if(!Validate.Required(text: lastname, Label: "last name")){return;}
        if(!Validate.Required(text: gender, Label: "gender")){return;}
        
        let sm = SignupModel()
        sm.email = UserName;
        sm.password = Password;
        sm.first_name = firstname;
        sm.last_name = lastname;
        sm.gender = gender;
        sm.special_offers = specialOffer;
        
        ApiHelper.Signup(sm) { (flag) in
            if(flag){
                PageRedirect.DashboardPage(viewController: self);
            }else{
                UtilityHelper.AlertMessage("Signup failed")
            }
        }
        
        
        
    }
    
    @IBAction func setGender(){
        
        ActionSheetStringPicker.show(withTitle: "Gender", rows: ["Male","Female"], initialSelection: genderSelectionIndex, doneBlock: { (picker, index, data) in
            //
            self.genderSelectionIndex = index;
            if(index == 0){
                self.tbxGender.text = "Male"
            }else{
                self.tbxGender.text = "Female"
            }
            
        }, cancel: { (picker) in
            //
            
        }, origin: tbxGender)
        
        
    }
    
    
}

extension SignupVC{
    
    func loadVisuals(){
        
        let image = StyleHelper.MaterialImage(name: "arrow.back", color: Colors.lightgray, width: 30, height: 30);
        backBtn.setImage(image, for: .normal);
        backBtn.tintColor = StyleHelper.colorWithHexString(Colors.lightgray)
        
        btnLogin.ThemeType1();
        StyleHelper.setFontImageVisualsFontAwesome(imgProfile, name: "usercircleo", color: Colors.lightgray);
        
        tbxUserName.setBorderBottom(color: Colors.lightgray, height: 1);
        tbxPassword.setBorderBottom(color: Colors.lightgray, height: 1);
        tbxFirstName.setBorderBottom(color: Colors.lightgray, height: 1);
        tbxLastName.setBorderBottom(color: Colors.lightgray, height: 1);
        tbxGender.setBorderBottom(color: Colors.lightgray, height: 1);
        
        StyleHelper.setFontImageVisualsFontAwesome(imgCheckbox, name: "check", color: Colors.lightgray);
        
    }
    
}

