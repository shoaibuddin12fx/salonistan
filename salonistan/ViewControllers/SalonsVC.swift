//
//  SalonsVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 22/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation


class SalonsVC: BaseVC, CLLocationManagerDelegate {
    //
    
    @IBOutlet weak var tableView: UITableView!
    var salons: [SalonModel] = [SalonModel]();
    var stylist: [StylistModel] = [StylistModel]();
    var serviceId: Int = 1;
    var service: ServiceModel!
    var locationManager:CLLocationManager!;
    var showHeader: Bool = false;
    
    private var filteredSalonList = [SalonModel]()
    private var filteredStylistList = [StylistModel]()
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        initTable();
        setupSearchController();
        loadData();
        initHeaderItems();
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, true)
//        }();
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, false)
//    }
//
    
    func initHeaderItems(){
        
        
        let img = StyleHelper.MaterialImage(name: "search", color: Colors.white, width: 30, height: 30);
        
        let menuBackItem: UIBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.showSearch))
        menuBackItem.tintColor = StyleHelper.colorWithHexString(Colors.white);
        
        self.navigationItem.rightBarButtonItems = [ menuBackItem ]
    }
    
    
    
    func loadData(){
        
        guard let _id = self.service.id else { return }
        
        self.serviceId = Int(_id);
        self.title = self.service.name;
        ApiHelper.SalonsOfService(id: serviceId) { (flag, salons, stylists) in
            //
            if flag, let a = salons, let b = stylists{
                self.salons = a;
                self.filteredSalonList = a
                self.stylist = b;
                self.filteredStylistList = b;
                self.tableView.reloadData();
            }
            
        }
        
    }
    
    func goback(){
        self.navigationController?.popViewController(animated: true);
    }
    
    
    
    func returnedLocation(location: CLLocation) {
        //
        print("Here I am");
        
    }
    
    
    
    
    
    
}

extension SalonsVC: UISearchBarDelegate, UISearchResultsUpdating{
    
    
    
    func setupSearchController(){
        searchController.searchBar.delegate = self;
        searchController.searchResultsUpdater = self
        searchController.searchBar.scopeButtonTitles = ["Relevance", "Lowest Price", "Highest Price", "Nearest"]
        searchController.searchBar.showsCancelButton = true;
        let cancelButtonAttributes: [NSAttributedStringKey: AnyObject] = [NSAttributedString.Key(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white]
        
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(cancelButtonAttributes, for: .normal)
        
        
        searchController.searchBar.resignFirstResponder();
        // searchController.dimsBackgroundDuringPresentation = true
        searchController.hidesNavigationBarDuringPresentation = true;
        // definesPresentationContext = true
        UtilityHelper.hideTopBar(v: searchController, true);
        // //self.searchbar = searchController.searchBar
        searchController.searchBar.barTintColor = StyleHelper.colorWithHexString(Colors.red)
        searchController.searchBar.tintColor = UIColor.white
        
    }
    
    @objc func showSearch(){
        self.showHeader = !self.showHeader;
        
        //searchController.isActive = !self.showHeader;
        if(self.showHeader){
            UtilityHelper.hideTopBar(v: self, true)
            tableView.tableHeaderView = searchController.searchBar
        }else{
            UtilityHelper.hideTopBar(v: self, false)
            tableView.tableHeaderView = nil
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        //
        // filterSearchController(searchBar);
        searchBarSearchButtonClicked(searchBar);
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        //
        // filterSearchController(searchController.searchBar)
    }
    
    func filterSearchController(_ searchBar: UISearchBar){
        
        let searchText = searchBar.text ?? ""
        filteredSalonList = salons.filter { items in
            let item = items
            let isMatchingSearchText =    item.name.lowercased().contains(searchText.lowercased()) || searchText.lowercased().count == 0
            return isMatchingSearchText
        }

        filteredStylistList = stylist.filter { items in
            let item = items
            let isMatchingSearchText = item.get_stylist.name.lowercased().contains(searchText.lowercased()) || searchText.lowercased().count == 0
            return isMatchingSearchText
        }

        tableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //
        print(searchBar.text);
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //
        print(searchBar.text);
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //
        print("cancel clicked");
        
        if(searchBar.text == "" || searchBar.text == nil){
            showSearch()
        }else{
            searchBar.text = "";
            return
        }
        
        
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //
        
        guard let scopeString = searchBar.scopeButtonTitles?[searchBar.selectedScopeButtonIndex] else { return }
        
        print(scopeString);
        let filter = scopeString.lowercased().replacingOccurrences(of: " ", with: "");
        print(filter);
        
        if(filter == "relevance"){
            
            ApiHelper.SalonsOfService(id: serviceId) { (flag, salons, stylists) in
                //
                if flag, let a = salons, let b = stylists{
                    self.salons = a;
                    self.stylist = b;
                    self.filterSearchController(searchBar);
                    searchBar.resignFirstResponder();
                    self.searchController.isActive = false;
                    
                }
                
            }
            
        }else{
            
            
            if(filter == "nearest" && !LocationHelper.shared.checkStatus()){
                
                searchController.searchBar.selectedScopeButtonIndex = 0
                UtilityHelper.AlertMessage("Please allow location permission");
                return;
            }
            
            
            
            
            
            
            
            ApiHelper.SalonsByFilter(id: serviceId, filter: filter) { (flag, salons) in
                //
                let searchText = searchBar.text ?? ""
                if let s = salons{
                    self.filteredSalonList = s.filter { items in
                        let item = items
                        let isMatchingSearchText =    item.name.lowercased().contains(searchText.lowercased()) || searchText.lowercased().count == 0
                        return isMatchingSearchText
                    }
                    
                    self.filteredStylistList = self.stylist.filter { items in
                        let item = items
                        let isMatchingSearchText = item.get_stylist.name.lowercased().contains(searchText.lowercased()) || searchText.lowercased().count == 0
                        return isMatchingSearchText
                    }
                    self.tableView.reloadData();
                    searchBar.resignFirstResponder();
                    self.searchController.isActive = false;
                }
                
            }
            
        }
        
        
        
        
        
        
        
        
        
    }
    
    
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestLocation()
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    
}






extension SalonsVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "SalonsTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "SalonsTVC")
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return tableView.frame.width * (256 / 370) + 20
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 2;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        
        if(section == 0){
            return searchController.isActive ? self.filteredSalonList.count : self.salons.count
        }else{
            return searchController.isActive ? self.filteredStylistList.count : self.stylist.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalonsTVC") as! SalonsTVC
        
        if(indexPath.section == 0){
            
            let item = searchController.isActive ? self.filteredSalonList[indexPath.row] : self.salons[indexPath.row]
            cell.setData(item)
            
        }else{
            
            let item = searchController.isActive ? self.filteredStylistList[indexPath.row] : self.stylist[indexPath.row]
            cell.setData(item)
            
        }
        
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        if(indexPath.section == 0){
            
            if let id = self.salons[indexPath.row].id{
                print(id)
                ApiHelper.featuresOfSalon(id: id as! Int) { (flag, salon) in
                    if flag, let s = salon{
                        PageRedirect.SubSalon(viewController: self, salon: s, stylist: nil, isStylist: false)
                    }
                }
            }
            
            
        }else{
            
            if let _stylist = self.stylist[indexPath.row].get_stylist, let id = _stylist.id{
                print(id)
                ApiHelper.featuresOfStylist(id: id as! Int) { (flag, stylist) in
                    if flag, let s = stylist{
                        PageRedirect.SubSalon(viewController: self, salon: nil, stylist: s, isStylist: true)
                    }
                }
            }
            
        }
        
        
    }
    
    
    
    
}
