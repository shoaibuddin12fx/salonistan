//
//  SearchVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 16/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class SearchVC: BaseVC {
    //
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchbar: UISearchBar!
    
    
    
    var category: [CategoryModel] = [CategoryModel]()
    var service: [ServiceModel] = [ServiceModel]()
    var salon: [SalonModel] = [SalonModel]()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.initTable();
        loadData();
        searchbar.delegate = self;
        searchbar.becomeFirstResponder();
        
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor = StyleHelper.colorWithHexString(Colors.red);
            
        }
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        // UtilityHelper.hideTopBar(v: self, true)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        // UtilityHelper.hideTopBar(v: self, false)
//    }
    
    func loadData(_ text: String = "search"){
        ApiHelper.Search(text: text) { (flag, category, service, salon) in
            //
            if let a = category, let b = service, let c = salon{
                self.category = a;
                self.service = b;
                self.salon = c;
                self.tableView.reloadData();
            }
            
            
            
            
        }
    }
    
    
    
    
    
    
    
}

extension SearchVC: UISearchBarDelegate{
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //
        print(searchBar.text);
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //
        print(searchBar.text);
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //
        print("cancel clicked");
        
        if(searchBar.text == "" || searchBar.text == nil){
            
            self.navigationController?.popViewController(animated: true);
        }else{
            searchBar.text = "";
            return
        }
        
        
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //
        print("search clicked");
        
        if let s = searchBar.text{
            loadData(s);
        }
        
        
        
    }
    
    
}



extension SearchVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "SearchTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "SearchTVC")
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 40.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return 40;
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 3;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        
        if(section == 0){
            return self.category.count;
        }else if(section == 1){
            return self.salon.count;
        }else{
            return self.category.count;
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTVC") as! SearchTVC
        
        // 1- 10 service , 11 - 20 salon , 21 - 30 category
        let section = indexPath.section
        if(section == 0){
            cell.setData(self.category[indexPath.row], number: section)
        }else if(section == 1){
            cell.setData(self.salon[indexPath.row])
        }else{
            cell.setData(self.category[indexPath.row], number: section)
        }
        
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
    }
    
    
    
    
}

