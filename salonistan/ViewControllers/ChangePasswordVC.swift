//
//  ChangePasswordVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 25/03/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class ChangePasswordVC: BaseVC {
    //
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet var lockImage: [UIImageView]!
    
    @IBOutlet weak var oldtxt: UITextField!
    @IBOutlet weak var newtxt: UITextField!
    @IBOutlet weak var retypetxt: UITextField!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        //
        let _ : Void = {
            UtilityHelper.hideTopBar(v: self, false)
        }();
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UtilityHelper.hideTopBar(v: self, true)
    }
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Change password"
        
        submitBtn.ThemeType1();
        
        for item in lockImage {
            
            StyleHelper.setFontImageVisualsFontAwesome(item, name: "lock", color: Colors.red)
            
        }
        
        
    }
    
    
    @IBAction func submitFOrm(_ sender: UIButton) {
        
        let oldt = self.oldtxt.text! as String;
        let newt = self.newtxt.text! as String;
        let rett = self.retypetxt.text! as String;
        
        if(!Validate.Required(text: oldt, Label: "old password")){return;}
        if(!Validate.Required(text: newt, Label: "new password")){return;}
        if(!Validate.Required(text: rett, Label: "retype password")){return;}
        
        if(!Validate.MatchStrings(Text1: newt, Text2: rett, Label1: "new password", Label2: "retype password")){return;}

        ApiHelper.ChangePassword(oldt, newt) { (flag) in
            if(flag){
                self.navigationController?.popViewController(animated: true);
            }
        }
        
        
            
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
}
