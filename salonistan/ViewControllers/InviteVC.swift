//
//  InviteVC.swift
//  salonistan
//
//  Created by Xtreme Hardware on 04/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//
import Foundation
import UIKit

class InviteVC: BaseTabVC, UITabBarControllerDelegate {
    //
    
   
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Invite Friends";
        initTabbarItems();
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, false)
//        }();
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, true)
//    }
    
    
    
    
    
    // delegates
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true;
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        // store tab index if needed
    }
}

class ItemVC: UIViewController {
    //
    
    @IBOutlet weak var btnInvite: UIButton!
    
    @IBOutlet weak var btnShare: UIButton!
    
    override func viewDidLoad() {
        //
        
        btnInvite.setBackgroundColor(color: Colors.red)
        btnInvite.roundBorder(color: Colors.red, width: 1)
        
        btnShare.setBackgroundColor(color: Colors.white)
        btnShare.roundBorder(color: Colors.red, width: 1)

        
        let img = StyleHelper.FontAwesomeImage(name: "sharealt", color: Colors.red, width: 30, height: 30)
        btnShare.setImage(img, for: .normal)
        
    }
    
    @IBAction func share(){
        UtilityHelper.shareYourLink(self);
    }
    
}

class  Item1VC: UIViewController {
    //
    
    @IBOutlet weak var tableView : UITableView!
    
    override func viewDidLoad() {
        //
        initTable();
    }
    
    
}


extension InviteVC{
    
    func initTabbarItems(){
        
        let imgtab1 = StyleHelper.FontAwesomeImage(name: "users", color: Colors.white, width: 30, height: 30);
        let imgtab2 = StyleHelper.FontAwesomeImage(name: "question", color: Colors.white, width: 30, height: 30);
        
        self.tabBar.items![0].image = imgtab1;
        self.tabBar.items![1].image = imgtab2;
        
        self.tabBar.items![0].title = "INVITE FRIENDS";
        self.tabBar.items![1].title = "FAQ";
        
        
        
        self.tabBar.tintColor = StyleHelper.colorWithHexString(Colors.red);
        self.tabBar.barTintColor = StyleHelper.colorWithHexString(Colors.white);
        self.tabBar.unselectedItemTintColor = StyleHelper.colorWithHexString(Colors.gray);
        
        //
        
    }
    
    
    
    
}

extension Item1VC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "SalonDescriptionTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "SalonDescriptionTVC")
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 40.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        
        return 1
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return 4
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalonDescriptionTVC") as! SalonDescriptionTVC
        
        cell.textView.text = "After completion of an appointment booked with Vaniday, you will receive an email to rate the service and write a review about the salon. Your review will then also be visible to other users. Yo can also always contact our customer service team - we would love to hear from you and are here to help.";
        
        let index = indexPath.row
        switch index {
        case 0:
            cell.textHeading.text = "How does it Work?"
            
            break
        case 1:
            cell.textHeading.text = "How can i invite friends?"
            
            break
        case 2:
            cell.textHeading.text = "What are the benefits of referring a friend?"
            
            break
        case 3:
           cell.textHeading.text = "Do i get one voucher code for each of the completed booking made by my best friends using my referral code?"
            
            break
        default:
            break
            
            
        }
        
        
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
    }
    
    
    
    
}

