//
//  AppointmentsTabVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 19/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class AppointmentsTabVC: BaseTabVC, UITabBarControllerDelegate {
    //
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Appointments";
        initTabbarItems();
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, false)
//        }();
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, true)
//    }
    
    // delegates
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true;
        
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        // store tab index if needed
    }
    
    
    
}

extension AppointmentsTabVC{
    
    func initTabbarItems(){
        
        let imgtab1 = StyleHelper.FontAwesomeImage(name: "calendarchecko", color: Colors.white, width: 30, height: 30);
        let imgtab2 = StyleHelper.FontAwesomeImage(name: "calendartimeso", color: Colors.white, width: 30, height: 30);
        
        self.tabBar.items![0].image = imgtab1;
        self.tabBar.items![1].image = imgtab2;
        
        self.tabBar.items![0].title = "Current";
        self.tabBar.items![1].title = "Previous";
        
        
        
        self.tabBar.tintColor = StyleHelper.colorWithHexString(Colors.red);
        self.tabBar.barTintColor = StyleHelper.colorWithHexString(Colors.white);
        self.tabBar.unselectedItemTintColor = StyleHelper.colorWithHexString(Colors.gray);
        
        //
        
    }
    
    
    
    
}













