//
//  BaseVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 07/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class BaseVC: UIViewController {
    //
    override func viewDidLoad() {
        //
        // solid color
        createBackButton();
        self.navigationController?.navigationBar.barTintColor = StyleHelper.colorWithHexString(Colors.red);
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func createBackButton(){
        
        if ((self.navigationController?.viewControllers.count)!>1){
            
            let img = StyleHelper.MaterialImage(name: "arrow.back", color: Colors.white, width: 30, height: 30);
            
            let menuBackItem: UIBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(BaseVC.popOutController))
            menuBackItem.tintColor = StyleHelper.colorWithHexString(Colors.white);
            self.navigationItem.leftBarButtonItem = menuBackItem
            
            //NO Bottom Border required in Back Screen
            navigationController?.navigationBar.barStyle = UIBarStyle.default
        }
        else
        {
            navigationController?.navigationBar.barStyle = UIBarStyle.default
        }
        
    }
    
    @objc func popOutController(){
        self.navigationController?.popViewController(animated: true);
    }
}

class BaseTabVC: UITabBarController {
    //
    override func viewDidLoad() {
        //
        // solid color
        createBackButton();
        self.navigationController?.navigationBar.barTintColor = StyleHelper.colorWithHexString(Colors.red);
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func createBackButton(){
        
        if ((self.navigationController?.viewControllers.count)!>1){
            
            let img = StyleHelper.MaterialImage(name: "arrow.back", color: Colors.white, width: 30, height: 30);
            
            let menuBackItem: UIBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(BaseVC.popOutController))
            menuBackItem.tintColor = StyleHelper.colorWithHexString(Colors.white);
            self.navigationItem.leftBarButtonItem = menuBackItem
            
            //NO Bottom Border required in Back Screen
            navigationController?.navigationBar.barStyle = UIBarStyle.default
        }
        else
        {
            navigationController?.navigationBar.barStyle = UIBarStyle.default
        }
        
    }
    
    @objc func popOutController(){
        self.navigationController?.popViewController(animated: true);
    }
}
