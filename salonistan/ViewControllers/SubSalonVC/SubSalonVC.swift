//
//  AddCostVC.swift
//  inventory360
//
//  Created by Osama Ahmed on 18/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class SubSalonVC: BaseVC {
    //
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionView2: UICollectionView!;
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var mainTopView: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    // view after image
    
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var starVIew: CosmosView!
    @IBOutlet weak var lblStar: UILabel!
    
    // bottom cart
    @IBOutlet weak var imgCart: UIImageView!
    @IBOutlet weak var lblBookcount: UILabel!
    @IBOutlet weak var lblPriceCount: UILabel!
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var viewCart: UIView!
    @IBOutlet weak var viewBanner: UIView!
    
    //
    @IBOutlet weak var secondaryImage: UIImageView!
    @IBOutlet weak var secBtnLeft: UIButton!
    @IBOutlet weak var secBtnRight: UIButton!
    @IBOutlet weak var secondaryView: UIView!
    
    
    var bookingArray: [AssociatedService] = [AssociatedService]();
    
    @IBAction func goBooking(_ sender: UIButton) {
        PageRedirect.DateTimeSelectorPage(viewController: self, booking: self.bookingArray);
    }
    
    
    
    //
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func goShare(_ sender: UIButton) {
        PageRedirect.InvitePage(viewController: self);
    }
    
    
    
    
    
    
    
    
    var isStylist = false;
    var singleSalon: SalonModel!;
    var singleStylist: StylistDetailModel!;
    
    var selectedIndex: Int = 0;
    var colArray: [[String:Any]] = [[String:Any]]();
    var bannerArray: [BannerModel] = [BannerModel]();
    var option: [String:Any]? = nil;
    var RPageVC: UIMPageVC!;
    
    @IBAction func goReview(_ sender: UIButton) {
        
        if(!isStylist){
            if let salon = singleSalon{
                PageRedirect.ReviewPage(viewController: self, id: salon.id as! Int, type: "salon")
            }
        }else{
            if let stylist = singleStylist{
                PageRedirect.ReviewPage(viewController: self, id: stylist.id as! Int, type: "stylist")
            }
            
        }
        
        
    }
    
    override func viewDidLoad() {
        //
        super.viewDidLoad()
        initHeaderItems();
        initCollectionView();
        loadData();
        
        
        
        let image = StyleHelper.FontAwesomeImage(name: "envelopeo", color: Colors.white, width: 30, height: 30);
        secBtnLeft.setImage(image, for: .normal);
        secBtnLeft.tintColor = StyleHelper.colorWithHexString(Colors.white)

        let image2 = StyleHelper.FontAwesomeImage(name: "phone", color: Colors.white, width: 30, height: 30);
        secBtnRight.setImage(image2, for: .normal);
        secBtnRight.tintColor = StyleHelper.colorWithHexString(Colors.white)
        
        secondaryImage.roundBorder(color: Colors.white, width: 2);
        
        
        StyleHelper.setFontImageVisualsFontAwesome(imgCart, name: "shoppingcart", color: Colors.white)
        lblBookcount.roundBorder(color: Colors.white, width: 1);
        
        
        
        lblComments.setBackgroundColor(color: Colors.red);
        lblComments.roundBorder(color: Colors.red, width: 1);
        lblComments.textColor = UIColor.white;
        StyleHelper.setFontImageVisualsFontAwesome(imgLike, name: "thumbsup", color: Colors.red);
        if(!isStylist){
            
            if let c = singleSalon.avgrating{
                starVIew.rating = Double(truncating: c as! NSNumber)
                lblStar.text = c.stringValue
            }
            
            
        }else{
            
            if let c = singleStylist.avgrating{
                starVIew.rating = Double(truncating: c as! NSNumber)
                lblStar.text = c.stringValue
            }
            
        }
        
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
        
        
        
        
        
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up");
            mainTopView.isHidden = true
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
            mainTopView.isHidden = false
        }
    }
    
    func initHeaderItems(){
        
        let img = StyleHelper.FontAwesomeImage(name: "sharealt", color: Colors.white, width: 30, height: 30);
        
        let menuBackItem: UIBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.showShare))
        menuBackItem.tintColor = StyleHelper.colorWithHexString(Colors.white);
        
        self.navigationItem.rightBarButtonItems = [ menuBackItem ]
        
    }
    
    @objc func showShare(){
        PageRedirect.InvitePage(viewController: self);
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //
        if segue.identifier == "SubSalonSegue" {
            self.RPageVC = segue.destination as! UIMPageVC;
            self.RPageVC.c_delegate = self;
            self.RPageVC.isStylist = self.isStylist;
            self.RPageVC.subSalon = self;
            
            if(!isStylist){
                self.RPageVC.singleSalon = self.singleSalon;
            }else{
                self.RPageVC.singleStylist = self.singleStylist;
            }
            
            
            
        }
    }
    
    func loadData(){
        
        secondaryView.isHidden = true;
        if(isStylist){
            self.title = self.singleStylist.name
            self.bannerArray = self.singleStylist.banners
        }else{
            self.title = self.singleSalon.name
            self.bannerArray = self.singleSalon.banners;
        }
        
        if(self.bannerArray.count == 0){
            secondaryView.isHidden = false;
            
            if(isStylist){
                if let img = self.singleStylist.image{
                    UtilityHelper.setImage(secondaryImage, img)
                }
                
            
            }else{
                
                if let img = self.singleSalon.image{
                    UtilityHelper.setImage(secondaryImage, img)
                }
                
                
            }
        }
        
        
        
        
        self.pageControl.numberOfPages = self.bannerArray.count;
        self.collectionView2.reloadData();
        viewCart.isHidden = (self.bookingArray.count <= 0) ? true : false;
        
    }
    
    
    
}

extension SubSalonVC: UIMPageVCDelegate{
    
    func sendScrollPosition(view: UIMPageVC, position: CGFloat) {
        //
        print(position);
        if(position <= mainTopView.frame.height ){
            mainScrollView.contentOffset.y = position;
        }
        
    }
    
    
    
    
    func setColViewSelection(view: UIMPageVC, index: Int) {
        //
        let ip = IndexPath(row: index, section: 0)
        self.collectionView.selectItem(at: ip, animated: true, scrollPosition: .centeredHorizontally);
    }
    
    func setbooking(view: UIMPageVC, col: AssociatedService, set: Bool) {
        //
        
        if(set){
            
            let flag = self.bookingArray.contains { (ser) -> Bool in
                return ser == col;
            }
            
            if(!flag){
                self.bookingArray.append(col);
            }
            
        }else{
            
            let flag = self.bookingArray.contains { (ser) -> Bool in
                return ser == col;
            }
            
            if(flag){
                
                let index = self.bookingArray.firstIndex { (ser) -> Bool in
                    return ser == col
                }
                
                if let i = index{
                    self.bookingArray.remove(at: i);
                }
                
            }
            
        }
        
        var t = Double(0.00);
        for e in self.bookingArray {
            if let st = e.serviceCost{
                t = t + (st as NSString).doubleValue
            }
        }
        
        self.lblBookcount.text = String(format: "%02d", self.bookingArray.count)
        self.lblPriceCount.text = String(format: "%.2f", t)
        viewCart.isHidden = (self.bookingArray.count <= 0) ? true : false;
        
    }
    
    
}

extension SubSalonVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func initCollectionView(){
        
        collectionView.register(UINib(nibName: "HeaderCVC", bundle: nil), forCellWithReuseIdentifier: "HeaderCVC");
        collectionView.delegate = self
        collectionView.dataSource = self;
        collectionView.allowsMultipleSelection = false;
        collectionView.setBorder(color: Colors.gray, radius: 5, width: 1);
        collectionView.reloadData();
        
        colArray = UtilityHelper.getPlistContent(name: "CSubSalonItems");
        
        if(isStylist){
            colArray.remove(at: 3)
        }
        
        
        
        
        collectionView2.register(UINib(nibName: "BannerCVC", bundle: nil), forCellWithReuseIdentifier: "BannerCVC");
        collectionView2.delegate = self
        collectionView2.dataSource = self;
        collectionView2.allowsMultipleSelection = false;
        collectionView2.reloadData();
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //
        
        
        collectionView.layoutIfNeeded();
        
        if(collectionView == self.collectionView){
            let height = (collectionView.frame.height);
            let width = CGFloat(100.0)
            return CGSize(width: width, height: height);
        }else{
            let height = (collectionView.frame.height);
            let width = (collectionView.frame.width)
            return CGSize(width: width, height: height);
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //
        
        
        // Dequeue a GridViewCell.
        if(collectionView == self.collectionView){
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: HeaderCVC.self), for: indexPath) as? HeaderCVC
                else { fatalError("unexpected cell in collection view") }
            
            if(self.colArray.count > 0){
                cell.setData(self.colArray[indexPath.row]["Label"] as! String)
            }
            
            
            return cell;
        }else{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: BannerCVC.self), for: indexPath) as? BannerCVC
                else { fatalError("unexpected cell in collection view") }
            
            if(self.bannerArray.count > 0){
                cell.setData(name: self.bannerArray[indexPath.row].image);
                self.pageControl.currentPage = ( indexPath.row + 1 );
            }
            
            
            return cell;
        }
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        //
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //
        if (collectionView == self.collectionView){
            return self.colArray.count;
        }else{
            return self.bannerArray.count;
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //
        return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //
        
        if(collectionView == self.collectionView){

            let cell = collectionView.cellForItem(at: indexPath) as! HeaderCVC;
            
            let i = self.RPageVC.currentVisibleIndex;
            if (i < indexPath.row){
                self.RPageVC.goToNextPage(index: indexPath.row)
            }else{
                self.RPageVC.goToPreviousPage(index: indexPath.row)
            }

        }else{
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //
        
        
        
    }
    
    
    
}
