//
//  UIRPageVC.swift
//  inventory360
//
//  Created by Osama Ahmed on 05/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

@objc protocol UIMPageVCDelegate{
    func setColViewSelection(view: UIMPageVC, index: Int);
    func setbooking(view: UIMPageVC, col: AssociatedService, set: Bool);
    func sendScrollPosition(view: UIMPageVC, position: CGFloat)
}


class UIMPageVC: UIPageViewController {
    //
    
    
    private(set) lazy var orderedViewControllers: [UIViewController] = [UIViewController]();
    var colArray: [[String:Any]] = [[ : ]];
    var currentVisibleIndex = 0;
    var isStylist = true;
    var singleSalon: SalonModel!;
    var singleStylist: StylistDetailModel!;
    weak var c_delegate:UIMPageVCDelegate?
    var subSalon: SubSalonVC!;
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.dataSource = self;
        self.delegate = self;
        self.isStylist = subSalon.isStylist;
        
    }
    
//    func setData(salon: SalonModel){
//        self.singleSalon = salon;
//        
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "salonfeaturedetail"), object: self.singleSalon, userInfo: nil)
//        
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        //
        
        let resourceInit : Void = {
            loadData();
        }();
        
    }
    
    func loadData(){
        colArray = UtilityHelper.getPlistContent(name: "CSubSalonItems");
//        if(subSalon.isStylist){
//            colArray.remove(at: 3)
//        }
        
        
        for item in colArray {
            orderedViewControllers.append(self.newColoredViewController(item["Key"] as! String))
        }
        
        let firstViewController = orderedViewControllers[currentVisibleIndex]
        setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil);
        
        self.c_delegate?.setColViewSelection(view: self, index: 0)
        
    }
    
    
    
    private func newColoredViewController(_ type: String) -> UIViewController {
        var con = UIViewController();
        
        switch type {
        case "FEA":
            let coni = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CSubFeaturedVC") as! CSubFeaturedVC;
            coni.Lparent = self;
            con = coni
            break;
        case "SER":
            let coni = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CSubServicesVC") as! CSubServicesVC;
            coni.Lparent = self;
            con = coni
            break;
        case "ABT":
            let coni = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CSubAboutVC") as! CSubAboutVC;
            coni.Lparent = self;
            con = coni
            break;
        case "STL":
            let coni = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CSubStylistVC") as! CSubStylistVC;
            coni.Lparent = self;
            con = coni
            break;
        case "POR":
            let coni = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CSubPortfolioVC") as! CSubPortfolioVC;
            coni.Lparent = self;
            con = coni
            break;
        
        default:
            break;
        }
        
        
        
        return con;
    }
    
    
    
    
}

extension UIMPageVC {
    
    func goToNextPage(index: Int){
        
        let nextViewController = self.orderedViewControllers[index];
        setViewControllers([nextViewController], direction: .forward, animated: false) { (flag) in
            if(flag){
                print(index);
            }
        }
        
        
        
    }
    
    
    func goToPreviousPage(index: Int){
        
        let previousViewController = self.orderedViewControllers[index]
        //setViewControllers([previousViewController], direction: .reverse, animated: false, completion: nil)
        setViewControllers([previousViewController], direction: .reverse, animated: false) { (flag) in
            if(flag){
                print(index);
            }
        }
        
    }
    
}

extension UIMPageVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        //
        if(finished){
            print(currentVisibleIndex);
            let n = pageViewController.viewControllers![0];
            let className : String = String(describing: n);
            if let trim = className.slice(from: "CSub", to: "VC"){
                let index = self.colArray.index { (item) -> Bool in
                    (item["Label"] as! String).lowercased() == trim.lowercased()
                }
                guard let n = index else {
                    return
                }
                self.c_delegate?.setColViewSelection(view: self, index: n);
                
            };
            
            
            
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        //
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else{
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        currentVisibleIndex = previousIndex;
        return orderedViewControllers[previousIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        //
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else{
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        currentVisibleIndex = nextIndex;
        return orderedViewControllers[nextIndex]
    }
    
    
    
    
    
    
    
    
}

