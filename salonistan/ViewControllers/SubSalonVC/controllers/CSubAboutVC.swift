//
//  AdRoCostVC.swift
//  inventory360
//
//  Created by Osama Ahmed on 19/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class CSubAboutVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!;
    var salon: SalonModel!
    var stylist: StylistDetailModel!
    var isStylist: Bool = false;
    
    //
    var Lparent: UIMPageVC!;
    
    override func viewDidLoad() {
        //
        initTable()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //
        self.isStylist = self.Lparent.isStylist;
        if(self.isStylist == true){
            self.stylist = self.Lparent.singleStylist;
            self.tableView.reloadData();
        }else{
            self.salon = self.Lparent.singleSalon;
            self.tableView.reloadData();
        }
        
        
    }
    
    
    
}

extension CSubAboutVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "SalonFeaturedTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "SalonFeaturedTVC");
        
        let nib2 = UINib(nibName: "SalonDescriptionTVC", bundle: nil);
        tableView.register(nib2, forCellReuseIdentifier: "SalonDescriptionTVC");
        
        let nib3 = UINib(nibName: "SalonsTVC", bundle: nil);
        tableView.register(nib3, forCellReuseIdentifier: "SalonsTVC");
        
        let nib4 = UINib(nibName: "SalonSocialTVC", bundle: nil);
        tableView.register(nib4, forCellReuseIdentifier: "SalonSocialTVC");
        
        
        
        
        
        
        
        tableView.dataSource = self;
        tableView.delegate = self;
        // tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return UITableViewAutomaticDimension
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        if(isStylist){
            return 2;
        }else{
            return 3;
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SalonDescriptionTVC") as! SalonDescriptionTVC
            
            if(self.isStylist){
                if let _des = stylist.Description{
                    cell.textView.text = _des
                }
                
            }else{
                
                if let _des = salon.Description{
                    cell.textView.text = _des
                }
                
            }
            
            
            return cell
            
        }else if(indexPath.section == 1){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SalonsTVC") as! SalonsTVC
            
            if(isStylist){
                cell.setData(self.Lparent.singleStylist);
            }else{
                cell.setData(self.Lparent.singleSalon);
            }
            
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SalonSocialTVC") as! SalonSocialTVC
            
            
            cell.setData(self.Lparent.singleSalon);
            
            return cell
            
            
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
        
    }
    
    
    
    
}
