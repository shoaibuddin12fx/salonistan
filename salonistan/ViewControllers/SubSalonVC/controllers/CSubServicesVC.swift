//
//  AdRoCostVC.swift
//  inventory360
//
//  Created by Osama Ahmed on 19/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class CSubServicesVC: UIViewController {
    
    @IBOutlet weak var RView: UIView!;
    @IBOutlet weak var tableView: UITableView!;
    var associated_services: [AssociatedService] = [AssociatedService]()
    
    
    //
    var Lparent: UIMPageVC!;
    
    override func viewDidLoad() {
        //
        initTable()
        RView.setBorder(color: Colors.gray, radius: 10, width: 1)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //
        if(self.Lparent.isStylist == true){
            
            self.associated_services = self.Lparent.singleStylist.associated_services;
            self.tableView.reloadData();
        }else{
            self.associated_services = self.Lparent.singleSalon.associated_services;
            self.tableView.reloadData();
        }
        
        let i = [Int]();
        for it in self.Lparent.subSalon.bookingArray{
            let ind = self.associated_services.firstIndex { (al) -> Bool in
                return al == it;
            }
            
            if let indexP = ind{
                self.tableView.selectRow(at: IndexPath(row: indexP, section: 0), animated: true, scrollPosition: .middle);
            }
        }
        
        
    }
    
    
    
}

extension CSubServicesVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "SalonFeaturedTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "SalonFeaturedTVC")
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
        tableView.allowsMultipleSelection = true;
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return 70
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return self.associated_services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalonFeaturedTVC") as! SalonFeaturedTVC
        
        cell.setData(self.associated_services[indexPath.row], flag: false)
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        self.Lparent.c_delegate?.setbooking(view: self.Lparent, col: self.associated_services[indexPath.row], set: true);
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //
        self.Lparent.c_delegate?.setbooking(view: self.Lparent, col: self.associated_services[indexPath.row], set: false);
    }
    
    
    
    
}
