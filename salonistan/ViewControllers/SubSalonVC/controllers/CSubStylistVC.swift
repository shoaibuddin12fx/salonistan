//
//  AdRoCostVC.swift
//  inventory360
//
//  Created by Osama Ahmed on 19/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class CSubStylistVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!;
    var salon: SalonModel!
    var associated_stylists: [AssociatedStylist] = [AssociatedStylist]()
    
    
    //
    var Lparent: UIMPageVC!;
    
    override func viewDidLoad() {
        //
        initTable()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //
        if(self.Lparent.isStylist == false){
            self.salon = self.Lparent.singleSalon;
            self.associated_stylists = self.salon.associated_stylists;
            self.tableView.reloadData();
        }
        
        
    }
    
    
    
}

extension CSubStylistVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "SalonStylistTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "SalonStylistTVC");
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return 100.0
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return self.associated_stylists.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SalonStylistTVC") as! SalonStylistTVC
            
            cell.setData(self.associated_stylists[indexPath.row]);
            
            return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
        
    }
    
    
    
    
}
