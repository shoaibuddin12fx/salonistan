//
//  AdRoCostVC.swift
//  inventory360
//
//  Created by Osama Ahmed on 19/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import INSPhotoGallery

class CSubPortfolioVC: UIViewController {
    //
    var Lparent: UIMPageVC!;
    @IBOutlet weak var tableView: UITableView!
    var albums: [AlbumModel] = [AlbumModel]();
    
    override func viewDidLoad() {
        //
        initTable();
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //
        if(self.Lparent.isStylist == true){
            self.albums = self.Lparent.singleStylist.albums;
            self.tableView.reloadData();
        }else{
            self.albums = self.Lparent.singleSalon.albums;
            self.tableView.reloadData();
        }
        
        
    }
    
    
}

extension CSubPortfolioVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "GalleryTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "GalleryTVC")
        tableView.dataSource = self;
        tableView.delegate = self;
        //tableView.estimatedRowHeight = 120.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        //
//        return 30.0
//    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //
        if let str = self.albums[section].name{
            return str;
        }else{
            return "";
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return UITableViewAutomaticDimension;
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return self.albums.count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GalleryTVC") as! GalleryTVC
        
        cell.setData(self.albums[indexPath.section])
        cell.delegate = self;
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
    }
    
    
    
    
}

extension CSubPortfolioVC: GalleryTVCDelegate{
    
    func openGallery(view: GalleryTVC, index: Int, inv: [INSPhotoViewable]) {
        //
        
        
        let currentPhoto = inv[index]
        let galleryPreview = INSPhotosViewController(photos: inv, initialPhoto: currentPhoto, referenceView: view)
        
        galleryPreview.referenceViewForPhotoWhenDismissingHandler = { [weak self] photo in
            
            return nil
        }
        
        present(galleryPreview, animated: true, completion: nil)
        
    }
    
    
}



