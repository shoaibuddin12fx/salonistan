//
//  DateTimeSelectorVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 10/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import FSCalendar

class DateTimeSelectorVC: BaseVC {
    //
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnSkip: UIButton!;
    @IBOutlet weak var Calender: FSCalendar!
    @IBOutlet weak var timePicker: UIDatePicker!
    
    var gDate: Date = Date();
    var subSalonVC: SubSalonVC!;
    var bookingArray: [AssociatedService] = [AssociatedService]();
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.title = "Select Date / Time"
        loadVIsuals();
        self.timePicker.date = Date();
        loadData();
        
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        patchTime();
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, false)
//        }();
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, true)
//    }
    
    func patchTime(){
        var components = Calendar.current.dateComponents([.hour, .minute], from: self.timePicker.date);
        components.hour = (components.hour ?? 0) + 1
        components.minute = 0
        
        let calendar = Calendar.current
        let date = calendar.date(from: components)
        self.timePicker.date = date!;
        
    }
    
    
    
    
    func loadVIsuals(){
        btnSkip.ThemeType1();
    }
    
    func loadData(){
        Calender.delegate = self;
        Calender.dataSource = self;
    }
    
    @IBAction func setDateTIme(){
        
        let _t = UtilityHelper.getCurrentDateTime(date: self.timePicker.date);
        
        var d_components = Calendar.current.dateComponents([.day, .month, .year ], from: gDate);
        var t_components = Calendar.current.dateComponents([.hour, .minute ], from: self.timePicker.date);
        var r_component = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute, .second ], from: Date() )
        r_component.day = d_components.day
        r_component.month = d_components.month
        r_component.year = d_components.year
        r_component.hour = t_components.hour
        r_component.minute = t_components.minute
        r_component.second = 0
        
        if var r_date = Calendar.current.date(from: r_component){
            r_date = UtilityHelper.getCurrentDateTime(date: r_date)
            print(r_date);
            
            if(r_date < UtilityHelper.getCurrentDateTime()){
                UtilityHelper.AlertMessage("Appointments in previous date / time not possible");
                return
            }
            
            
        };
        
        
        
        
        if let d = d_components.day, let m = d_components.month, let y = d_components.year,
            let h = t_components.hour, let mi = t_components.minute{
            
            let final_date = "\(String(format: "%.2d", d))-\(String(format: "%.2d", m))-\(y)";
            let final_time = "\(String(format: "%.2d", h)):\(String(format: "%.2d", mi)):00";
            
            print(final_date, final_time);
            
            if(self.subSalonVC.isStylist == true){
                PageRedirect.FinalBookingPage(viewController: self, salonVC: self.subSalonVC, booking: self.bookingArray, date: final_date, time: final_time, stylist: nil)
            }else{
                PageRedirect.SelectStylistPage(viewController: self, salonVC: self.subSalonVC, booking: self.bookingArray, date: final_date, time: final_time);
                
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
        }
        
        
        
        
        
        
    }
    
}

extension DateTimeSelectorVC{
    
}

extension DateTimeSelectorVC: FSCalendarDelegate, FSCalendarDataSource{
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date();
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height;
        self.view.layoutIfNeeded();
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        //
        print(date);
        self.gDate = date;
    }
    
}


