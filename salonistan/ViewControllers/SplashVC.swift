//
//  SplashVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 08/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class SplashVC: UIViewController {
    //
    override func viewDidLoad() {
        //
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //
        let _ : Void = {
            //UtilityHelper.hideTopBar(v: self, true)
            checkLoginSequence()
        }();
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //UtilityHelper.hideTopBar(v: self, false)
    }
    
    func checkLoginSequence(){
        
        if(SettingsDataHelper.checkIfSettingsExist()){
            let settings = SettingsDataHelper.returnSettings() as? Settings;
            let flag = settings?.skipTutorial;
            
            if let f = flag , f == true{
                PageRedirect.DashboardPage(viewController: self)
            }else{
                PageRedirect.PreSignupPage(viewController: self);
            }
            
            
            
        }else{
            PageRedirect.PreSignupPage(viewController: self);
        }
        
        
        
        
        
        
//        UtilityHelper.ShowLoader();
        // simulate login here
        // will check if user already logged in
//        let user = UserDataHelper.returnUser();
//        if((user) != nil){
//
//            UtilityHelper.HideLoader();
//            PageRedirect.DashboardPage(viewController: self);
//
//        }else{
//            UtilityHelper.HideLoader();
//            // do if here
//            PageRedirect.PreLoginPage(viewController: self);
//
//            //PageRedirect.LoginPage(viewController: self);
//        }
//
        
    }
    
    
}
