//
//  FinalBookingVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 11/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import ActionSheetPicker_3_0

class FinalBookingVC: BaseVC, BtnBookingDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    var final_date: String!
    var final_time: String!
    var subSalonVC: SubSalonVC!;
    var isStylist: Bool = false;
    var selectedStylist: AssociatedStylist!
    var bookingArray: [AssociatedService] = [AssociatedService]();
    var paymentSelectionIndex = 0;
    var paymentSections: [String] = ["Pay at Salon","Online Payment"];
    var total_price: Double = 0.0;
    var total_hours: Int = 0;
    var total_minutes: Int = 0;
    var serviceIds: String = ""
    
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Appointment Details"
        initTable();
        self.isStylist = self.subSalonVC.isStylist
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, false)
//        }();
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, true)
//    }
    
    func clicked(view: BtnBooking) {
        // finalize data here;
        
        guard let user = UserDataHelper.returnUser() else {
            UtilityHelper.AlertMessage("You must logged in first")
            return
        }
        
        
        
        
        
        let postdata = [
            "isStylist": subSalonVC.isStylist,
            "clientId" : user.id,
            "serviceIds" : self.serviceIds as! String,
            "staffId" : (isStylist) ? 0 : selectedStylist.id,
            "date" : final_date as! String,
            "time" : final_time as! String,
            "hours" : self.total_hours,
            "minutes" : 0,
            "statusId" : 0,
            "price" : self.total_price,
            "payment_method": paymentSelectionIndex
        ] as! [String:Any]
        
        print(postdata);
        
        let bid = (isStylist) ? self.subSalonVC.singleStylist.id : self.subSalonVC.singleSalon.id
        
        ApiHelper.SetAppointment(shopid: bid as! Int, data: postdata) { (flag) in
            //
            if(flag){
                UtilityHelper.AlertMessage("Appointment successful");
                PageRedirect.DashboardPage(viewController: self);
            }else{
                
            }
        }
        
        
        
        
        
        
        
    }
    
    
}

extension FinalBookingVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib0 = UINib(nibName: "AppointmentCVC", bundle: nil);
        tableView.register(nib0, forCellReuseIdentifier: "AppointmentCVC")
        let nib1 = UINib(nibName: "PaymentCVC", bundle: nil);
        tableView.register(nib1, forCellReuseIdentifier: "PaymentCVC")
        let nib2 = UINib(nibName: "PrivacyCVC", bundle: nil);
        tableView.register(nib2, forCellReuseIdentifier: "PrivacyCVC")
        let nib3 = UINib(nibName: "DiscountCVC", bundle: nil);
        tableView.register(nib3, forCellReuseIdentifier: "DiscountCVC")
        let nib4 = UINib(nibName: "CashbackCVC", bundle: nil);
        tableView.register(nib4, forCellReuseIdentifier: "CashbackCVC")
        let nib5 = UINib(nibName: "BookingCVC", bundle: nil);
        tableView.register(nib5, forCellReuseIdentifier: "BookingCVC")
        let nib6 = UINib(nibName: "OtherCVC", bundle: nil);
        tableView.register(nib6, forCellReuseIdentifier: "OtherCVC")
        let nib7 = UINib(nibName: "BtnBooking", bundle: nil);
        tableView.register(nib7, forCellReuseIdentifier: "BtnBooking")
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20;
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return UITableViewAutomaticDimension;
//        switch indexPath.row {
//        case 0:
//            //
//            return 150;
//        case 1:
//            //
//            return 100;
//        case 2:
//            //
//            return 40;
//        case 3:
//            //
//            return 80;
//        case 4:
//            //
//            return 120;
//        case 5:
//            //
//            return 150;
//        case 6:
//            //
//            return 80;
//        case 7:
//            //
//            return 40;
//        default:
//            //
//            return 20;
//        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 8;
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        
        return 1;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            let cell0 = tableView.dequeueReusableCell(withIdentifier: "AppointmentCVC") as! AppointmentCVC
            //
            cell0.setBorder(color: Colors.white, radius: 15, width: 1);
            
            var str = "";
            var price = 0.0;
            var hh = 0;
            var sers: String = ""
            for e in bookingArray{
                if let n = e.service.name{
                    str = str + n + ", "
                }
                
                if let st = e.serviceCost{
                    price = price + (st as NSString).doubleValue
                }
                
                if let h = e.serviceDuration, h != nil, h != ""{
                    hh = hh + (h as NSString).integerValue;
                }
                
                
                sers = sers + "\(e.serviceId.stringValue)" + ",";
                
                
            }
            
            self.serviceIds = String(sers.dropLast())
            
            
            cell0.Service.text = str;
            cell0.price.text = "AED \(price)"
            self.total_price = price;
            self.total_hours = hh;
            
            cell0.Date.text = final_date;
            cell0.Time.text = final_time;
            if(isStylist){
                cell0.Location.text = subSalonVC.singleStylist.name + "\n" + subSalonVC.singleStylist.address
            }else{
                cell0.Location.text = subSalonVC.singleSalon.name + "\n" + subSalonVC.singleSalon.address
            }
            
            
            
            
            
            
            
            return cell0;
            break;
        case 1:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "PaymentCVC") as! PaymentCVC
            //
            cell1.setBorder(color: Colors.white, radius: 15, width: 1)
            return cell1;
            break;
        case 2:
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "PrivacyCVC") as! PrivacyCVC
            //
            cell2.setBorder(color: Colors.white, radius: 15, width: 1)
            return cell2;
            break;
        case 3:
            let cell3 = tableView.dequeueReusableCell(withIdentifier: "DiscountCVC") as! DiscountCVC
            //
            cell3.setBorder(color: Colors.white, radius: 15, width: 1)
            return cell3;
            break;
        case 4:
            let cell4 = tableView.dequeueReusableCell(withIdentifier: "CashbackCVC") as! CashbackCVC
            //
            cell4.setBorder(color: Colors.white, radius: 15, width: 1)
            return cell4;
            break;
        case 5:
            let cell5 = tableView.dequeueReusableCell(withIdentifier: "BookingCVC") as! BookingCVC
            //
            cell5.setBorder(color: Colors.white, radius: 15, width: 1);
            cell5.setData(self.bookingArray);
            return cell5;
            break;
        case 6:
            let cell6 = tableView.dequeueReusableCell(withIdentifier: "OtherCVC") as! OtherCVC
            //
            cell6.setBorder(color: Colors.white, radius: 15, width: 1)
            return cell6;
            break;
        case 7:
            let cell7 = tableView.dequeueReusableCell(withIdentifier: "BtnBooking") as! BtnBooking
            //
            cell7.selectionStyle = .none
            cell7.delegate = self;
            return cell7;
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCVC") as! OtherCVC
            //
            
            
            
            return cell;
            break
        }
        
        
        
        
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
        if(indexPath.section == 1){
            
            let cell1 = tableView.cellForRow(at: indexPath) as! PaymentCVC
            
            
            ActionSheetStringPicker.show(withTitle: "Payment Method", rows: paymentSections, initialSelection: self.paymentSelectionIndex, doneBlock: { (picker, index, data) in
                //
                self.paymentSelectionIndex = index;
                cell1.hdng3.text = self.paymentSections[self.paymentSelectionIndex]
                
                tableView.deselectRow(at: indexPath, animated: true);
                
                
                
                
            }, cancel: { (picker) in
                //
                
            }, origin: tableView)
            
        }
        
    }
    
    
    
    
}












