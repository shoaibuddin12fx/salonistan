//
//  EditProfileVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 28/03/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import ActionSheetPicker_3_0


class EditProfileVC: BaseVC, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    //
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var tbxFirstName: UITextField!
    @IBOutlet weak var tbxLastName: UITextField!
    @IBOutlet weak var tbxUserName: UITextField!
    @IBOutlet weak var tbxPhone: UITextField!
    
    var imagePicker = UIImagePickerController()
    var pimg: UIImage? = nil;
    var base64Image = "";
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();    
        self.title = "Signup"
        loadVisuals();
        
        imgProfile.isUserInteractionEnabled = true;
        let taps = UITapGestureRecognizer.init(target: self, action: #selector(self.uploadinage(_:) ) );
        imgProfile.addGestureRecognizer(taps);
        setData()
        
    }
    
    @objc func uploadinage(_ recognizer: UITapGestureRecognizer){
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
        }
        
        
    }
    
    func setData(){
        ApiHelper.GetProfile { (flag, user) in
            if(flag){
                print(user);
                if let _user = user {
                
                    self.tbxFirstName.text = _user.firstName
                    self.tbxLastName.text = _user.lastName;
                    self.tbxUserName.text = _user.email;
                    self.tbxPhone.text = _user.mobileNo;
                    
                    if let _img = _user.profilePic{
                       UtilityHelper.setImage(self.imgProfile, _img)
                    }
                    
                    
                    
                    
                    
                    
                    
                }
                
            }else{
                UtilityHelper.AlertMessage("Error finding your profile");
                self.navigationController?.popViewController(animated: true);
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            self.imgProfile.image = pickedImage;
            pimg = pickedImage
        }
        
        dismiss(animated: true, completion: {
            //
            
            
            if let _b64img = self.pimg, let data = UIImageJPEGRepresentation( _b64img, 1){
                
                self.base64Image = data.base64EncodedString(options: .lineLength64Characters) ;
                
                
            };
            
            
            
        })
        
    }
    
    
    
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true);
    }
    
    
    @IBAction func signup(_ sender: Any){
        
        // Initializing Items
        let UserName: String = tbxUserName.text!  as String;
        let firstname: String = tbxFirstName.text! as String;
        let lastname: String = tbxLastName.text! as String;
        let phone: String = tbxPhone.text! as String
        
        //doing Validations
        if(!Validate.Required(text: UserName, Label: "email or phone")){return;}
        if(!Validate.Required(text: firstname, Label: "first name")){return;}
        if(!Validate.Required(text: lastname, Label: "last name")){return;}
        if(!Validate.Required(text: phone, Label: "phone")){return;}
        
        let sm = [
            "email" : UserName,
            "first_name" : firstname,
            "last_name" : lastname,
            "mobileNo" : phone,
            "image" : ( (base64Image != "") ? base64Image : nil )
            ] as [String : Any]
        
        ApiHelper.EditProfle(sm) { (flag) in
            if(flag){
                UtilityHelper.AlertMessage("Profile updated")
            }else{
                UtilityHelper.AlertMessage("Can't update profile right now")
            }
            
            self.navigationController?.popViewController(animated: true);
        }
        
        
        
    }
    
    @IBAction func btnClicked() {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
}

extension EditProfileVC{
    
    func loadVisuals(){
        
        let image = StyleHelper.MaterialImage(name: "arrow.back", color: Colors.lightgray, width: 30, height: 30);
        backBtn.setImage(image, for: .normal);
        backBtn.tintColor = StyleHelper.colorWithHexString(Colors.lightgray)
        
        btnLogin.ThemeType1();
        StyleHelper.setFontImageVisualsFontAwesome(imgProfile, name: "usercircleo", color: Colors.lightgray);
        imgProfile.roundBorder(color: Colors.lightgray, width: 1);
        
        tbxUserName.setBorderBottom(color: Colors.lightgray, height: 1);
        tbxFirstName.setBorderBottom(color: Colors.lightgray, height: 1);
        tbxLastName.setBorderBottom(color: Colors.lightgray, height: 1);
        
        
    }
    
}

