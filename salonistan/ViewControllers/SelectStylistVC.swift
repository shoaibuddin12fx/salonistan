//
//  SelectStylistVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 17/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class SelectStylistVC: BaseVC{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var btnSkip: UIButton!;
    var associatedStylist: [AssociatedStylist] = [AssociatedStylist]();
    var selectedIndexPath = 0;
    var final_date: String!
    var final_time: String!
    var subSalonVC: SubSalonVC!;
    var bookingArray: [AssociatedService] = [AssociatedService]();
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Select Stylist";
        initTable();
        btnSkip.ThemeType1();
        let stylist = AssociatedStylist();
        stylist.get_stylist = StylistDetailModel()
        stylist.get_stylist.name = "Any Stylist";
        stylist.get_stylist.speciality = SpecialityModel()
        stylist.get_stylist.speciality.name = "Any";
        stylist.get_stylist.reviewCount = 0
        
        stylist.get_stylist.avgrating = 0
        
        self.associatedStylist = subSalonVC.singleSalon.associated_stylists
        self.associatedStylist.insert(stylist, at: 0);
        self.tableView.reloadData();
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, false)
//        }();
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, true)
//    }
    
    @IBAction func setStylist(){
        
        PageRedirect.FinalBookingPage(viewController: self, salonVC: self.subSalonVC, booking: self.bookingArray, date: self.final_date, time: self.final_time, stylist: self.associatedStylist[selectedIndexPath])
        
    }
    
    
}

extension SelectStylistVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "SalonStylistTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "SalonStylistTVC")
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return 120
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return self.associatedStylist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalonStylistTVC") as! SalonStylistTVC
        
        cell.setData( self.associatedStylist[indexPath.row])
        
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        self.selectedIndexPath = indexPath.row;
        
    }
    
    
    
    
}
