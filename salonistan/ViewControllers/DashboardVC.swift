//
//  DashboardVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 09/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class DashboardVC: UIViewController {
    
    //
    @IBOutlet weak var searchVIew: UIView!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var categories: [CategoryModel] = [CategoryModel]();
    var sidemenu: SidemenuVC!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        //
        initTable();
        loadVisuals();
        loadData();
        initSidemenuNtifications()
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        //
        let _ : Void = {
            UtilityHelper.hideTopBar(v: self, true)
        }();
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UtilityHelper.hideTopBar(v: self, false)
    }
    
    @IBAction func logout(_ sender: UIButton){
        UserDataHelper.setLogout();
        PageRedirect.SplashPage(viewController: self);
    }
    
    func loadVisuals(){
        searchVIew.roundBorder(color: Colors.white, width: 1)
        let img = StyleHelper.FontAwesomeImage(name: "bars", color: Colors.red, width: 30, height: 30)
        btnMenu.setImage(img, for: .normal)
        btnMenu.tintColor = StyleHelper.colorWithHexString(Colors.red)
    }
    
    func loadData(){
        
        ApiHelper.Categories { (flag, data) in
            if let d = data, flag != false {
                self.categories = d;
                self.tableView.reloadData();
            }
        }
        
    }
    
    @IBAction func loadSearch(){
        PageRedirect.SearchPage(viewController: self);
    }
    
    
    @IBAction func loadSidemenu(){
        
        sidemenu = storyboard?.instantiateViewController(withIdentifier: "SidemenuVC") as! SidemenuVC;
        sidemenu.view.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        sidemenu.view.tag = 9876;
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        _window.layer.add(transition, forKey: nil)
        _window.addSubview((self.sidemenu?.view)!);
        
    }
    
    func initSidemenuNtifications(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(fromSidebar(_:)), name: NSNotification.Name(rawValue: "sm"), object: nil);
       
    }
    
    @objc func fromSidebar(_ notification: Notification) {
        
        let data: Int = notification.object as! Int;
        guard let key = data as? Int else { return };
        print(key);
        
        guard let sb = self.sidemenu else {return}
        _window.viewWithTag(9876)?.removeFromSuperview()
        self.sidemenu = nil;
        
        switch key {
        case 0:
            // login
            PageRedirect.LoginPage(viewController: self);
            break;
        case 1:
            // logout
            UserDataHelper.setLogout();
            // PageRedirect.LoginPage(viewController: self);
            break;
        case 2:
            // home
            
            break;
        case 3:
            // appointments
            if(UserDataHelper.checkIfUserExist()){
                PageRedirect.AppointmentsPage(viewController: self);
            }else{
                UtilityHelper.AlertMessage("You must be logged in first")
            }
            
            
            
            
            
            break;
        case 4:
            // invite friends
            PageRedirect.InvitePage(viewController: self);
            break;
        case 5:
            // settings
            PageRedirect.SettingsPage(viewController: self);
            break;
        default:
            //
            break
        }
        
    }
    
    
}

extension DashboardVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "CategoriesTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "CategoriesTVC")
        
        let nib2 = UINib(nibName: "BannerCollectionTVC", bundle: nil);
        tableView.register(nib2, forCellReuseIdentifier: "BannerCollectionTVC")
        
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        if(indexPath.section == 0){
            return tableView.frame.width * ( 9 / 16 )
        }else{
            return tableView.frame.width / 2
        }
        
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 2;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        if(section == 0){
            return 1;
        }else{
            return self.categories.count;
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCollectionTVC") as! BannerCollectionTVC
            
            cell.setData()
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTVC") as! CategoriesTVC
            
            cell.setData(c: self.categories[indexPath.row])
            
            return cell
        }
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        print(self.categories[indexPath.row]);
        PageRedirect.ServicesPage(viewController: self, cat: self.categories[indexPath.row])
    }
    
    
    
    
}


