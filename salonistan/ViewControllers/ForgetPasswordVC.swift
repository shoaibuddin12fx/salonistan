//
//  ForgetPasswordVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 18/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class ForgetPasswordVC: BaseVC {
    //
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tbxUserName: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        loadVisuals();
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func LoginPressed(sender: UIButton) {
        
        // Initializing Items
        let UserName: String = tbxUserName.text!  as String;
        
        //doing Validations
        if(!Validate.Required(text: UserName, Label: "email")){return;}
        ApiHelper.ForgetPassword(UserName) { (flag) in
            //
            if(flag){
                
            }else{
                
            }
        }
        
        
    }
}

extension ForgetPasswordVC{
    
    func loadVisuals(){
        backBtn.isHidden = true;
        let image = StyleHelper.MaterialImage(name: "arrow.back", color: Colors.lightgray, width: 30, height: 30);
        backBtn.setImage(image, for: .normal);
        backBtn.tintColor = StyleHelper.colorWithHexString(Colors.lightgray)
        btnLogin.ThemeType1();
        
        StyleHelper.setFontImageVisualsFontAwesome(imgProfile, name: "usercircleo", color: Colors.lightgray);
        tbxUserName.setBorderBottom(color: Colors.lightgray, height: 1);
        
    }
    
}
