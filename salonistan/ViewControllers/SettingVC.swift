//
//  SettingVC.swift
//  salonistan
//
//  Created by Xtreme Hardware on 06/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class SettingVC: BaseVC, UIGestureRecognizerDelegate {
//


    @IBOutlet weak var img1: UIImageView!
    
    @IBOutlet weak var img2: UIImageView!
    
    @IBOutlet weak var img3: UIImageView!
    
    @IBOutlet weak var img4: UIImageView!
    
    @IBOutlet weak var img5: UIImageView!
    
    @IBOutlet var btnGroup: [UIButton]!
    
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Settings"
        loadVisuals();
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, false)
//        }();
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, true)
//    }
    
    
    func loadVisuals(){
        
        StyleHelper.setFontImageVisualsFontAwesome(img1, name: "key", color: Colors.red);
        StyleHelper.setFontImageVisualsFontAwesome(img2, name: "edit", color: Colors.red);
        StyleHelper.setFontImageVisualsFontAwesome(img3, name: "infocircle", color: Colors.red);
        StyleHelper.setFontImageVisualsFontAwesome(img4, name: "lock", color: Colors.red);
       
        StyleHelper.setFontImageVisualsFontAwesome(img5, name: "filetext", color: Colors.red);
    }
    
    @IBAction func onClick(sender: UIButton){
        
        let tag = sender.tag
        
        switch tag {
        case 0:
            //
            PageRedirect.ChangePasswordPage(viewController: self)
            break;
        case 1:
            //
            PageRedirect.EditProfilePage(viewController: self);
            break;
            
        case 2:
            openUrlInBrowser(name: "help")
            break;
        case 3:
            openUrlInBrowser(name: "policy")
            break;
        case 4:
            openUrlInBrowser(name: "terms")
            break;
            
            
        default:
            break;
        }
        
        
    
    }
    
    func openUrlInBrowser(name: String){
        
        guard let url = URL(string: "http://salonistan.com/"+name) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    
    
    
    
    
    
}
