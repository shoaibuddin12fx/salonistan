//
//  CurrentAppointmentVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 19/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class CurrentAppointmentVC: UIViewController {
    //
    @IBOutlet weak var tableView: UITableView!
    var appointments: [AppointmentModel] = [AppointmentModel]();
    
    
    override func viewDidLoad() {
        //
        initTable();
        loadData();
        
    }
    
    func loadData(){
        ApiHelper.GetCurrentAppointments { (flag, model) in
            //
            if flag , let m = model{
                self.appointments = m;
                self.tableView.reloadData();
            }
            
        }
    }
}

extension CurrentAppointmentVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "CPAppTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "CPAppTVC")
        
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return UITableViewAutomaticDimension
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return self.appointments.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CPAppTVC") as! CPAppTVC
        cell.setData(self.appointments[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detail = self.appointments[indexPath.row];
        let str = (detail.isStylist != nil) && (detail.isStylist == 1) ? "stylist" : "salon"
        PageRedirect.AppointmentDetailPage(viewController: self, ap_id: detail.id as! Int, ap_type: str)
        
        
    }
    
    
    
    
}

