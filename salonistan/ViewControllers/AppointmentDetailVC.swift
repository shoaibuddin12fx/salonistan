//
//  AppointmentDetailVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 29/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import Foundation
import UIKit


class AppointmentDetailVC: BaseVC, BtnBookingDelegate {
    //
    
    @IBOutlet weak var mainTopView: UIView!
    @IBOutlet weak var collectionView2: UICollectionView!;
    @IBOutlet weak var pageControl: UIPageControl!
    
    //
    @IBOutlet weak var secondaryImage: UIImageView!
    @IBOutlet weak var secBtnLeft: UIButton!
    @IBOutlet weak var secBtnRight: UIButton!
    @IBOutlet weak var secondaryView: UIView!
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var ap_id: Int = 0;
    var ap_type: String = "salon";
    var ap_detail_model: AppointmentDetailModel!
    var final_date: String!
    var final_time: String!
    var selectedSalon: SalonModel!;
    var isStylist: Bool = false;
    var selectedStylist: StylistDetailModel!
    var bookingArray: [AssociatedService] = [AssociatedService]();
    var paymentSections: [String] = ["Pay at Salon","Online Payment"];
    var total_price: Double = 0.0;
    var total_hours: Int = 0;
    var total_minutes: Int = 0;
    var serviceIds: String = ""
    var bannerArray: [BannerModel] = [BannerModel]();
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Appointment Details";
        initCollectionView();
        
        
        let image = StyleHelper.FontAwesomeImage(name: "envelopeo", color: Colors.white, width: 30, height: 30);
        secBtnLeft.setImage(image, for: .normal);
        secBtnLeft.tintColor = StyleHelper.colorWithHexString(Colors.white)
        
        let image2 = StyleHelper.FontAwesomeImage(name: "phone", color: Colors.white, width: 30, height: 30);
        secBtnRight.setImage(image2, for: .normal);
        secBtnRight.tintColor = StyleHelper.colorWithHexString(Colors.white)
        
        secondaryImage.roundBorder(color: Colors.white, width: 2);
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeUp.direction = .up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //
        loadData();
        
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left")
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.up {
            print("Swipe Up");
            mainTopView.isHidden = true
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.down {
            print("Swipe Down")
            mainTopView.isHidden = false
        }
    }
    
    func loadData(){
        
        ApiHelper.GetAppointment(id: ap_id, type: ap_type) { (flag, ap_model) in
            if(flag){
                self.initTable();
                self.isStylist = (self.ap_type == "salon") ? false : true;
                self.ap_detail_model = ap_model;
                self.selectedSalon = self.ap_detail_model.get_salon;
                self.selectedStylist = self.ap_detail_model.get_stylist;
                self.final_date = self.ap_detail_model.date
                self.final_time = self.ap_detail_model.time;
                self.bookingArray = self.ap_detail_model.associated_services
                self.tableView.reloadData();
                
                self.secondaryView.isHidden = true;
                if(self.isStylist){
                    self.bannerArray = self.selectedStylist.banners
                }else{
                    self.bannerArray = self.selectedSalon.banners;
                }
                
                if(self.bannerArray.count == 0){
                    self.secondaryView.isHidden = false;
                    
                    if(self.isStylist){
                        if let img = self.selectedStylist.image{
                            UtilityHelper.setImage(self.secondaryImage, img)
                        }
                        
                        
                    }else{
                        
                        if let img = self.selectedSalon.image{
                            UtilityHelper.setImage(self.secondaryImage, img)
                        }
                        
                        
                    }
                }
                
                
                
                
                self.pageControl.numberOfPages = self.bannerArray.count;
                self.collectionView2.reloadData();
                

            }
        }
        
    }
    
    func clicked(view: BtnBooking) {
        // finalize data here;
        self.navigationController?.popViewController(animated: true);
        
    }
    
    
}

extension AppointmentDetailVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib0 = UINib(nibName: "AppointmentCVC", bundle: nil);
        tableView.register(nib0, forCellReuseIdentifier: "AppointmentCVC")
        let nib1 = UINib(nibName: "PaymentCVC", bundle: nil);
        tableView.register(nib1, forCellReuseIdentifier: "PaymentCVC")
        let nib2 = UINib(nibName: "PrivacyCVC", bundle: nil);
        tableView.register(nib2, forCellReuseIdentifier: "PrivacyCVC")
        let nib3 = UINib(nibName: "DiscountCVC", bundle: nil);
        tableView.register(nib3, forCellReuseIdentifier: "DiscountCVC")
        let nib4 = UINib(nibName: "CashbackCVC", bundle: nil);
        tableView.register(nib4, forCellReuseIdentifier: "CashbackCVC")
        let nib5 = UINib(nibName: "BookingCVC", bundle: nil);
        tableView.register(nib5, forCellReuseIdentifier: "BookingCVC")
        let nib6 = UINib(nibName: "OtherCVC", bundle: nil);
        tableView.register(nib6, forCellReuseIdentifier: "OtherCVC")
        let nib7 = UINib(nibName: "BtnBooking", bundle: nil);
        tableView.register(nib7, forCellReuseIdentifier: "BtnBooking")
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20;
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return UITableViewAutomaticDimension;
        //        switch indexPath.row {
        //        case 0:
        //            //
        //            return 150;
        //        case 1:
        //            //
        //            return 100;
        //        case 2:
        //            //
        //            return 40;
        //        case 3:
        //            //
        //            return 80;
        //        case 4:
        //            //
        //            return 120;
        //        case 5:
        //            //
        //            return 150;
        //        case 6:
        //            //
        //            return 80;
        //        case 7:
        //            //
        //            return 40;
        //        default:
        //            //
        //            return 20;
        //        }
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return (ap_detail_model != nil) ? 6 : 0;
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        
        return (ap_detail_model != nil) ? 1 : 0;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell0 = tableView.dequeueReusableCell(withIdentifier: "AppointmentCVC") as! AppointmentCVC
            //
            cell0.setBorder(color: Colors.white, radius: 15, width: 1);
            
            var str = "";
            var price = 0.0;
            var hh = 0;
            var sers: String = ""
                
            for e in self.bookingArray{
                if let n = e.service.name{
                    str = str + n + ", "
                }
                
                if let st = e.serviceCost{
                    price = price + (st as NSString).doubleValue
                }
                
                if let h = e.serviceDuration, h != nil, h != ""{
                    hh = hh + (h as NSString).integerValue;
                }
                
                
                sers = sers + "\(e.id.stringValue)" + ",";
                
                
            }
            
            self.serviceIds = String(sers.dropLast())
                
            
            
            
            
            cell0.Service.text = str;
            cell0.price.text = "AED \(price)"
            self.total_price = price;
            self.total_hours = hh;
            
            cell0.Date.text = final_date;
            cell0.Time.text = final_time;
            if(isStylist){
                cell0.Location.text = self.selectedStylist.name + "\n" + self.selectedStylist.address
            }else{
                cell0.Location.text = self.selectedSalon.name + "\n" + self.selectedSalon.address
            }
            
            
            
            
            
            
            
            return cell0;
            break;
        case 1:
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "PaymentCVC") as! PaymentCVC
            //
            cell1.setBorder(color: Colors.white, radius: 15, width: 1)
            return cell1;
            break;
        case 2:
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "PrivacyCVC") as! PrivacyCVC
            //
            cell2.setBorder(color: Colors.white, radius: 15, width: 1)
            return cell2;
            break;
        case 3:
            let cell3 = tableView.dequeueReusableCell(withIdentifier: "DiscountCVC") as! DiscountCVC
            //
            cell3.setBorder(color: Colors.white, radius: 15, width: 1);
            cell3.subhdng.text = "No discounts added."
            return cell3;
            break;
        case 4:
            let cell5 = tableView.dequeueReusableCell(withIdentifier: "BookingCVC") as! BookingCVC
            //
            cell5.setBorder(color: Colors.white, radius: 15, width: 1);
            cell5.setData(self.bookingArray);
            return cell5;
            break;
        case 5:
            let cell7 = tableView.dequeueReusableCell(withIdentifier: "BtnBooking") as! BtnBooking
            //
            cell7.selectionStyle = .none
            cell7.delegate = self;
            cell7.btn.setTitle("CLOSE", for: .normal);
            return cell7;
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCVC") as! OtherCVC
            //
            
            
            
            return cell;
            break
        }
        
        
        
        
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
    }
    
    
    
    
}

extension AppointmentDetailVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func initCollectionView(){
        
        collectionView2.register(UINib(nibName: "BannerCVC", bundle: nil), forCellWithReuseIdentifier: "BannerCVC");
        collectionView2.delegate = self
        collectionView2.dataSource = self;
        collectionView2.allowsMultipleSelection = false;
        collectionView2.reloadData();
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //
        
        
        collectionView.layoutIfNeeded();
        
        let height = (collectionView.frame.height);
        let width = (collectionView.frame.width)
        return CGSize(width: width, height: height);
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //
        
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: BannerCVC.self), for: indexPath) as? BannerCVC
            else { fatalError("unexpected cell in collection view") }
        
        if(self.bannerArray.count > 0){
            cell.setData(name: self.bannerArray[indexPath.row].image);
            self.pageControl.currentPage = ( indexPath.row + 1 );
        }
        
        
        return cell;
        
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        //
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //
        return self.bannerArray.count;
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        //
        return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        //
        
    }
    
    
    
}




