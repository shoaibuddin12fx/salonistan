//
//  ServiceVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 20/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class ServiceVC: BaseVC {
    //
    
    @IBOutlet weak var backBtn: UIButton!
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var topHeading: UILabel!
    
    var category: CategoryModel!
    var service: [ServiceModel] = [ServiceModel]();
    
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Services"
        UtilityHelper.setImage(topImageView, category.image);
        topHeading.text = category.name.capitalizingFirstLetter();
        initTable();
        loadData();
        loadVisuals();
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, false)
//        }();
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, true)
//    }
    
    func loadVisuals(){
        let image = StyleHelper.MaterialImage(name: "arrow.back", color: Colors.lightgray, width: 30, height: 30);
        backBtn.setImage(image, for: .normal);
        backBtn.tintColor = StyleHelper.colorWithHexString(Colors.white)
        
    }
    
    func loadData(){
        
        guard let id = category.id else { return }
        
        
        ApiHelper.ServicesOfCategory(id: Int(id)) { (flag, services) in
            //
            if flag, let s = services{
                self.service = s;
                self.tableView.reloadData();
            }
        }
    }
    
    
    
}

extension ServiceVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "SearchTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "SearchTVC")
        
        let nib1 = UINib(nibName: "ServiceHeaderTVC", bundle: nil);
        tableView.register(nib1, forHeaderFooterViewReuseIdentifier: "ServiceHeaderTVC")
        
        
        
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 40.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ServiceHeaderTVC") as! ServiceHeaderTVC;
        
        if let a = category.name{
            view.heading.text = "All \(a) Services"
        }
        
        
        return view
        
        
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40;
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return 40
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return self.service.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTVC") as! SearchTVC
        
        cell.setData(self.service[indexPath.row]);
        cell.colorView.isHidden = true;
        
        
        return cell
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        print(self.service[indexPath.row])
        let _s = self.service[indexPath.row]
        PageRedirect.SalonsOfService(viewController: self, service: _s);
        
        
    }
    
    
    
    
}

