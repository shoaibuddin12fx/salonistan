//
//  LoginVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 08/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import SwiftIconFont

class LoginVC: BaseVC {
    
    // variables
    
    // IBOutlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tbxUserName: UITextField!
    @IBOutlet weak var tbxPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var viewTwitterLogin: UIView!
    @IBOutlet weak var viewFacebookLogin: UIView!
    
    @IBOutlet weak var btnTwitterLogin: UIButton!
    @IBOutlet weak var btnFacebookLogin: UIButton!
    
    @IBOutlet weak var imgtwitterLogin: UIImageView!
    @IBOutlet weak var imgFacebookLogin: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    
    override func viewDidLoad() {
        //
        super.viewDidLoad();
        self.title = "Login"
        loadVisuals();
//        tbxUserName.text = "shoaibuddin12fx@gmail.com";
//        tbxPassword.text = "whatever";
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        //
//        let _ : Void = {
//            UtilityHelper.hideTopBar(v: self, true)
//        }();
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        UtilityHelper.hideTopBar(v: self, false)
//    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true);
    }
    
    //Login Events
    @IBAction func LoginPressed(sender: UIButton) {
        
        // Initializing Items
        let UserName: String = tbxUserName.text!  as String;
        let Password: String = tbxPassword.text! as String;
        
        //doing Validations
        if(!Validate.Required(text: UserName, Label: "email or phone")){return;}
        if(!Validate.Required(text: Password, Label: "password")){return;}
        
        ApiHelper.Login(UserName, Password) { (success) in
            if(success){
                
                print("login successful");
                PageRedirect.DashboardPage(viewController: self);
                
            }else{
                
                print("login failed");
                UtilityHelper.AlertMessage("Login Failed");
                
                
            }
        }
        
    }
    
    // Forget Password Event
    @IBAction func ForgetPasswordPressed(sender: UIButton) {
        PageRedirect.ForgetPage(viewController: self);
    }
    
    // Signup Event
    @IBAction func SignupPressed(sender: UIButton) {
        PageRedirect.SignupPage(viewController: self);
    }
    
    @IBAction func FacebookLogin(){
        GBHFacebookHelper.shared.login(controller: nil) { (success1, prompt) in
            if(success1){
                self.tryLogin();
            }
        }
    }
    
    fileprivate func tryLogin(){
        
        
        GBHFacebookHelper.shared.fbDataRequest(completion: { (success2, signup) in
            //
            if(success2){   
                
                let email = signup?.email;
                let password = settings.gPassword;
                
                print(signup);
                ApiHelper.Login(email!, password, completion: { (success3) in
                    //
                    if(success3){
                        PageRedirect.DashboardPage(viewController: self);
                    }else{
                        guard let sm = signup else {
                            UtilityHelper.AlertMessage(Errors.dbSignupFailed);
                            return
                        }
                        sm.password = password;
                        ApiHelper.Signup(sm, completion: { (success4) in
                            //
                            if(success4){
                                ApiHelper.Login(email!, password, completion: { (success5) in
                                    //
                                    if(success5){
                                        PageRedirect.DashboardPage(viewController: self)
                                    }else{
                                        UtilityHelper.AlertMessage(Errors.dbLoginFailed);
                                    }
                                })
                            }else{
                                UtilityHelper.AlertMessage(Errors.dbSignupFailed);
                            }
                        })
                    }
                })
                
                
            }else{
                UtilityHelper.AlertMessage(Errors.FbLoginFailed)
            }
            
        });
        
        
    }
    
    
    
    
    
    
    
    
}

struct Errors {
    static let FbLoginFailed = "401: Facebook Unreachable";
    static let dbLoginFailed = "402: Databse Unreachable";
    static let dbSignupFailed = "403: Signup failed";
    
}

extension LoginVC{
    
    func loadVisuals(){
        let image = StyleHelper.MaterialImage(name: "arrow.back", color: Colors.lightgray, width: 30, height: 30);
        backBtn.setImage(image, for: .normal);
        backBtn.tintColor = StyleHelper.colorWithHexString(Colors.lightgray)
        btnLogin.ThemeType1();
        viewTwitterLogin.roundBorder(color: Colors.twitterBlue, width: 1);
        viewTwitterLogin.setBackgroundColor(color: Colors.twitterBlue);
        viewFacebookLogin.roundBorder(color: Colors.facebookBlue, width: 1);
        viewFacebookLogin.setBackgroundColor(color: Colors.facebookBlue);
        StyleHelper.setFontImageVisualsFontAwesome(imgtwitterLogin, name: "twitter", color: Colors.white);
        StyleHelper.setFontImageVisualsFontAwesome(imgFacebookLogin, name: "facebook", color: Colors.white);
        StyleHelper.setFontImageVisualsFontAwesome(imgProfile, name: "usercircleo", color: Colors.lightgray);
        tbxUserName.setBorderBottom(color: Colors.lightgray, height: 1);
        tbxPassword.setBorderBottom(color: Colors.lightgray, height: 1);
        // imgProfile.roundBorder(color: Colors.gray, width: 1);
    }
    
}



