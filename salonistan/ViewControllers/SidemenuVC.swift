//
//  SidemenuVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 12/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit

class SidemenuVC: UIViewController, UIGestureRecognizerDelegate {
    //
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet var btnGroup: [UIButton]!
    
    // logged in view
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var btnCash: UIButton!
    
    @IBOutlet weak var settingsVIew: UIView!
    
    
    
    
    
    override func viewDidLoad() {
        //
        loadVisuals();
        addTapHideGesture();
        loginView.isHidden = true;
        logoutView.isHidden = true;
        settingsVIew.isHidden = true;
        
        
        
        
    }
    
    @IBAction func openLIckFOrCash(_ sender: Any) {
        guard let url = URL(string: "http://salonistan.com/") else { return }
        UIApplication.shared.open(url)
    }
    
    func addTapHideGesture(){
        let gesture = UITapGestureRecognizer();
        gesture.delegate = self;
        gesture.addTarget(self, action: #selector(tapit(_:)))
        self.view.addGestureRecognizer(gesture);
        
    }
    
    @objc func tapit(_ gesture: UITapGestureRecognizer){
        print("it tapped");
        
        
    }
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        print(touch.view?.tag);
        if let av = touch.view{
            if(av.tag == 20){
                _window.viewWithTag(9876)?.removeFromSuperview()
            }
        }
        
        
        return true;
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let flag = UserDataHelper.checkIfUserExist()
        
        if(flag){
            hideLoginView(true);
            if let user = UserDataHelper.returnUser(){
                if let fn = user.firstName, let ln = user.lastName{
                    lblProfileName.text = fn + " " + ln
                }
                
                
                
            }
            
            
        }else{
            hideLoginView(false);
        }
    }
    
    func hideLoginView(_ flag: Bool){
        loginView.isHidden = flag;
        logoutView.isHidden = !flag;
        settingsVIew.isHidden = !flag;
    }
    
    func loadVisuals(){
        
    StyleHelper.setFontImageVisualsFontAwesome(img1, name: "home", color: Colors.red);
    StyleHelper.setFontImageVisualsFontAwesome(img2, name: "calendarchecko", color: Colors.red);
    StyleHelper.setFontImageVisualsFontAwesome(img3, name: "users", color: Colors.red);
    StyleHelper.setFontImageVisualsFontAwesome(img4, name: "cog", color: Colors.red);
    
    btnLogout.roundBorder(color: Colors.white, width: 2);
    
    StyleHelper.setFontImageVisualsMaterial(logoImage, name: "person", color: Colors.white)
        
        btnCash.ThemeType1();
        btnLogin.ThemeType1();
        
    
        
    }
    
    @IBAction func actionSidemenu(_ sender: UIButton){
        
        let tag = sender.tag;
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sm"), object: tag, userInfo: nil)
        
    }
    
    
    
    
}



























