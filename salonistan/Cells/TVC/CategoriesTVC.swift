//
//  CategoriesTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 12/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit

class CategoriesTVC: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var heading: UILabel!
    
    override func prepareForReuse() {
        imgView.image = nil;
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(c: CategoryModel){
        self.heading.text = c.name.capitalizingFirstLetter();
        UtilityHelper.setImage(imgView, c.image);
        
        
        
    }
    
    
}
