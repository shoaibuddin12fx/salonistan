//
//  SalonsTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 22/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class Salons2TVC: UITableViewCell {

    @IBOutlet weak var imgVIew: UIImageView!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var subHeading1: UILabel!
    @IBOutlet weak var subHeading2: UILabel!
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var RView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //
        loadVisual();
        
    }
    
    
    func loadVisual(){
        
       
        StyleHelper.setFontImageVisualsFontAwesome(imgCard, name: "creditcard", color: Colors.red)
        
        
    }
    
    func setData(_ data: SalonModel ){
        
        if let i = data.image{
            UtilityHelper.setImage(imgVIew, i)
        }
        
        self.subHeading1.text = data.name;
        self.subHeading2.text = data.address;
    }
    
    func setData(_ data : StylistDetailModel){
        
        
        
        if let i = data.image{
            UtilityHelper.setImage(imgVIew, i)
        }
        
        self.subHeading1.text = data.name;
        self.subHeading2.text = data.address;
        
        
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
