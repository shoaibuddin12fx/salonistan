//
//  GalleryTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 01/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit
import INSPhotoGallery

@objc protocol GalleryTVCDelegate{
    func openGallery(view: GalleryTVC, index: Int, inv: [INSPhotoViewable]  );
}

class GalleryTVC: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var clHeight: NSLayoutConstraint!
    var photos: [PhotoModel] = [PhotoModel]();
    lazy var iphotos: [INSPhotoViewable] = [INSPhotoViewable]()
    weak var delegate:GalleryTVCDelegate?
    
    override func prepareForReuse() {
        //
        iphotos.removeAll();
    }
    
    override func awakeFromNib() {
        //
        initCollection();
    }
    
    func setData(_ data: AlbumModel){
        self.photos = data.photos;
        self.collectionView.reloadData();
        self.clHeight.constant = self.collectionView.collectionViewLayout.collectionViewContentSize.height;
    }
    
    
    
}

extension GalleryTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func initCollection(){
        
        let nib = UINib(nibName: "GalleryImageCVC", bundle: nil);
        collectionView.register(nib, forCellWithReuseIdentifier: "GalleryImageCVC")
        collectionView.dataSource = self;
        collectionView.delegate = self;
        collectionView.allowsMultipleSelection = false;
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //
        let width = collectionView.frame.width / 3;
        let height = collectionView.frame.height;
        return CGSize(width: width, height: width);
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryImageCVC", for: indexPath) as! GalleryImageCVC
        
        cell.setData(photos[indexPath.row].image);
        setAlbum(img: photos[indexPath.row].image);
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //
        self.delegate?.openGallery(view: self, index: indexPath.row, inv: iphotos)
    }
    
    func setAlbum(img: String){
        
        if img != "", let url = URL(string: api.IMAGEURL + img){
            let img = UIImage(named: "no")
            let nsurl = INSPhoto(imageURL: url, thumbnailImage: img);
            iphotos.append(nsurl);
        }
        
        
    }
    
}
