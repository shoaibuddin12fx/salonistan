//
//  SalonDescriptionTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 29/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit

class SalonDescriptionTVC: UITableViewCell {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textHeading: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
