//
//  SalonFeaturedTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 29/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit

class SalonFeaturedTVC: UITableViewCell {
    
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblSubHeading: UILabel!
    @IBOutlet weak var lblTime: UILabel!



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setData(_ data: AssociatedService, flag: Bool){
        if let i = data.service.icon, let s = data.serviceDuration{
            
            if(flag){
                UtilityHelper.setImage(mainImg, i);
                mainImg.contentMode = .scaleToFill
            }else{
                let im = StyleHelper.FontAwesomeImage(name: "dotcircleo", color: Colors.red, width: 30, height: 30);
                mainImg.image = im;
                mainImg.contentMode = .center
            }
            
            
            
            lblTime.text = "\(s ) Hours"
        }
        
        StyleHelper.setFontImageVisualsFontAwesome(iconImg, name: "infocircle", color: Colors.gray)
        
        lblHeading.text = data.service.name;
        lblSubHeading.text = data.serviceCost;
        
        
    }
    
    
    
    
}
