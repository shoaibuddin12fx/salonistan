//
//  SalonStylistTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 31/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit
import Cosmos

class SalonStylistTVC: UITableViewCell {

    @IBOutlet weak var imgVIew: UIImageView!
    @IBOutlet weak var subHeading1: UILabel!
    @IBOutlet weak var subHeading2: UILabel!
    @IBOutlet weak var imgBaloon: UIImageView!
    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var startRatingView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var RView: UIView!
    @IBOutlet weak var lblRating: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        RView.setBorder(color: Colors.lightgray, radius: 10, width: 1);
        imgVIew.roundBorder(color: Colors.white, width: 1)
        
        StyleHelper.setFontImageVisualsMaterial(imgBaloon, name: "chat.bubble", color: Colors.red);
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: AssociatedStylist){
        
        if let stylist = data.get_stylist{
            
            if let i = stylist.image, i != ""{
                UtilityHelper.setImage(imgVIew, i)
            }else{
                imgVIew.image = UIImage(named: "no");
            }
            
            
            
            self.subHeading1.text = stylist.name;
            self.subHeading2.text = stylist.speciality.name;
            lblCounter.text = stylist.reviewCount.stringValue
            
            if let c = stylist.avgrating{
                ratingView.rating = Double(truncating: c as! NSNumber)
                lblRating.text = String(format: "%.1f", c);
            }
            
            
            
        }
        
        
        
    }
    
    
    
}
