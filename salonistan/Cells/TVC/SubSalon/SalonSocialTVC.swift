//
//  SalonSocialTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 30/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit

class SalonSocialTVC: UITableViewCell {

    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: SalonModel ){
        
        if  let f = data.facebook,
            let g = data.google,
            let l = data.linkdin,
            let s = data.skype,
            let e = data.email{
            
            label1.text = "facebook: \t \(f)"
            label2.text = "google: \t\t \(g)"
            label3.text = "linkdin: \t\t \(l)"
            label4.text = "skype: \t\t \(s)"
            label5.text = "email: \t\t \(e)"
            
        }
        
        
        
    }
    
    
}
