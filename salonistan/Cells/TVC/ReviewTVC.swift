//
//  ReviewTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 18/02/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit
import Cosmos

class ReviewTVC: UITableViewCell {
    
    @IBOutlet weak var mainImg: UIImageView!;
    @IBOutlet weak var heading: UILabel!;
    @IBOutlet weak var subheading: UILabel!;
    @IBOutlet weak var startReview: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code;
        mainImg.roundBorder(color: Colors.gray, width: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(c: ReviewModel){
        self.heading.text = c.subject;
        self.subheading.text = c.message;
        self.startReview.rating = c.rating.doubleValue;
    }
    
}
