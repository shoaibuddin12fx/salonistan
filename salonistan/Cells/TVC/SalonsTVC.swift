//
//  SalonsTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 22/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

class SalonsTVC: UITableViewCell {

    @IBOutlet weak var imgVIew: UIImageView!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var subHeading1: UILabel!
    @IBOutlet weak var subHeading2: UILabel!
    @IBOutlet weak var imgBaloon: UIImageView!
    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var startRatingView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var RView: UIView!
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var stylistView: UIView!
    @IBOutlet weak var lblSpeciality: UILabel!
    @IBOutlet weak var imgVIew2: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //
        loadVisual();
        stylistView.isHidden = true;
        
    }
    
    override func prepareForReuse() {
        //
        imgVIew.image = nil;
        imgVIew2.image = UIImage(named: "no");
        
    }
    
    
    func loadVisual(){
        
        RView.setBorder(color: Colors.lightgray, radius: 10, width: 1);
        
        
        StyleHelper.setFontImageVisualsMaterial(imgBaloon, name: "chat.bubble", color: Colors.red);
        
        StyleHelper.setFontImageVisualsFontAwesome(imgCard, name: "creditcard", color: Colors.red)
        
        
    }
    
    func setData(_ data: SalonModel ){
        
        stylistView.isHidden = true;
        if let i = data.image{
            UtilityHelper.setImage(imgVIew, i)
        }
        
        
        self.subHeading1.text = data.name;
        self.subHeading2.text = data.address;
        lblCounter.text = String(data.reviews.count);
        
        if let c = data.rating, let d = Double(c){
            ratingView.rating = Double(truncating: d as! NSNumber)
            lblRating.text = String(d)
        }
        
        
            
            //data.avgrating.stringValue
        
    }
    
    func setData(_ data : StylistModel){
        
        stylistView.isHidden = false;
        guard let stylist = data.get_stylist else { return }
        if let i = stylist.image{
            imgVIew2.roundBorder(color: Colors.white, width: 2)
            //UtilityHelper.setImage(imgVIew, i);
            imgVIew.image = nil
            UtilityHelper.setImage(imgVIew2, i);
        }
        
        if let _speciality = stylist.speciality{
            self.lblSpeciality.text = _speciality.name.uppercased()
        }
        
        
        self.subHeading1.text = stylist.name;
        self.subHeading2.text = stylist.address;
        lblCounter.text = String(data.reviews.count);
        
        if let c = data.avgrating {
            ratingView.rating = Double(truncating: c as! NSNumber)
            lblRating.text = c.stringValue
        }
        
    }
    
    func setData(_ data : StylistDetailModel){
        
        stylistView.isHidden = false;
        let stylist = data
        if let i = stylist.image{
            imgVIew2.roundBorder(color: Colors.white, width: 2)
            //UtilityHelper.setImage(imgVIew, i);
            imgVIew.image = nil
            UtilityHelper.setImage(imgVIew2, i);
        }
        
        self.lblSpeciality.text = stylist.speciality.name.uppercased()
        self.subHeading1.text = stylist.name;
        self.subHeading2.text = stylist.address;
        lblCounter.text = String(data.reviews.count);
        
        if let c = data.avgrating {
            ratingView.rating = Double(truncating: c as! NSNumber)
            lblRating.text = c.stringValue
        }
        
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
