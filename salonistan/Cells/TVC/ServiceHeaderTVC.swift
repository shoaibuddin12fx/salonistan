//
//  ServiceHeaderTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 21/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit

class ServiceHeaderTVC: UITableViewHeaderFooterView {

    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var hView: UIView!;
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        //
        
        let color = StyleHelper.colorWithHexString(Colors.red)
        self.setBackgroundColor(color: Colors.white);
        heading.tintColor = color;
        heading.textColor = color;
        hView.setBackgroundColor(color: Colors.red)
        
        
        
        
        
    }
    
    
    
    

}
