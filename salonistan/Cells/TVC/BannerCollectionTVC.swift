//
//  BannerCollectionTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 17/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class BannerCollectionTVC: UITableViewCell {

    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    var banners: [BannerModel] = [BannerModel]();
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initCollection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(){
        
        ApiHelper.Banners { (flag, data) in
            if let d = data, flag != false {
                self.banners = d;
                self.pgControl.numberOfPages = self.banners.count;
                self.collectionView.reloadData();
            }
        }
        
    }
    
}

extension BannerCollectionTVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func initCollection(){
        
        let nib = UINib(nibName: "BannerCVC", bundle: nil);
        collectionView.register(nib, forCellWithReuseIdentifier: "BannerCVC")
        collectionView.dataSource = self;
        collectionView.delegate = self;
        collectionView.allowsMultipleSelection = false;
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //
        let width = collectionView.frame.width;
        let height = collectionView.frame.height;
        return CGSize(width: width, height: height);
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return banners.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCVC", for: indexPath) as! BannerCVC
        
        cell.setData(name: banners[indexPath.row].image);
        self.pgControl.currentPage = indexPath.row;
        
        return cell
        
    }
    
}
