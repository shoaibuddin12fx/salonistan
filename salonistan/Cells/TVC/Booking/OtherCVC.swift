//
//  OtherCVC.swift
//  salonistan
//
//  Created by Xtreme Hardware on 11/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class OtherCVC: UITableViewCell {
    @IBOutlet weak var hdng: UILabel!
    @IBOutlet weak var subHdng: UILabel!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        hdng.setBorderBottom(color: Colors.red, height: 1)
        label.setBorderBottom(color: Colors.lightgray, height: 1)
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
