//
//  PaymentCVC.swift
//  salonistan
//
//  Created by Xtreme Hardware on 11/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class PaymentCVC: UITableViewCell {

    @IBOutlet weak var hdng1: UILabel!
    @IBOutlet weak var hdng3: UILabel!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let img2 = StyleHelper.FontAwesomeImage(name: "money", color: Colors.red, width: 20, height: 20)
        self.img2.image = img2;
        
        let img3 = StyleHelper.FontAwesomeImage(name: "check", color: Colors.red, width: 20, height: 20)
        self.img3.image = img3;
        
        
        hdng1.setBorderBottom(color: Colors.red, height: 1)

        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
