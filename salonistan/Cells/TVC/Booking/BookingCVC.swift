//
//  BookingCVC.swift
//  salonistan
//
//  Created by Xtreme Hardware on 11/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class BookingCVC: UITableViewCell {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hdng: UILabel!
    var bookingArray: [AssociatedService] = [AssociatedService]();
    
    override func awakeFromNib() {
        super.awakeFromNib()
        hdng.setBorderBottom(color: Colors.red, height: 1);
        initTable();
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: [AssociatedService]){
        self.bookingArray = data;
        tableView.reloadData();
    }
    
    
    
}

extension BookingCVC: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func initTable(){
        let nib = UINib(nibName: "BookingPricesTVC", bundle: nil);
        tableView.register(nib, forCellReuseIdentifier: "BookingPricesTVC")
        
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.estimatedRowHeight = 100.0;
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView(frame: CGRect.zero);
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //
        return UITableViewAutomaticDimension
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //
        return 2;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        if(section == 0){
            return 1;
        }else{
            return self.bookingArray.count;
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingPricesTVC") as! BookingPricesTVC
        
        if(indexPath.section == 0){
            
            var price = 0.0;
            for e in bookingArray{
                if let st = e.serviceCost{
                    price = price + (st as NSString).doubleValue
                }
            }
            
            cell.setData(total: "\(price)");
            
            
        }else{
            
            cell.setData(self.bookingArray[indexPath.row])
            
            
        }
        
        return cell
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
    }
    
    
    
    
}

