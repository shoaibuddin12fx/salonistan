//
//  DiscountCVC.swift
//  salonistan
//
//  Created by Xtreme Hardware on 11/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class DiscountCVC: UITableViewCell {

    @IBOutlet weak var hdng: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var subhdng: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let img = StyleHelper.FontAwesomeImage(name: "infocircle", color: Colors.red, width: 20, height: 20)
        self.img.image = img;
        hdng.setBorderBottom(color: Colors.red, height: 1)
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
