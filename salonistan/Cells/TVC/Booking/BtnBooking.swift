//
//  BtnBooking.swift
//  salonistan
//
//  Created by Xtreme Hardware on 17/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

@objc protocol BtnBookingDelegate{
    func clicked(view: BtnBooking)
}

class BtnBooking: UITableViewCell {
    @IBOutlet weak var btn: UIButton!
    weak var delegate: BtnBookingDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btn.roundBorder(color: Colors.red, width: 1)
        btn.setBackgroundColor(color: Colors.red)
        
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clicked(){
        self.delegate?.clicked(view: self);
    }
    
}
