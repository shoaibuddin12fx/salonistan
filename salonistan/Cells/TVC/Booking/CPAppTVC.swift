//
//  CPAppTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 19/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class CPAppTVC: UITableViewCell {

    @IBOutlet weak var RView: UIView!
    @IBOutlet weak var imgVIew: UIImageView!
    @IBOutlet weak var subHeading1: UILabel!
    @IBOutlet weak var subHeading2: UILabel!
    @IBOutlet weak var imgPlan1: UIImageView!
    @IBOutlet weak var imgPlan2: UIImageView!
    @IBOutlet weak var lblC1: UILabel!
    @IBOutlet weak var lblC2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        RView.setBorder(color: Colors.lightgray, radius: 10, width: 1);
        imgVIew.roundBorder(color: Colors.white, width: 1)
        
        StyleHelper.setFontImageVisualsFontAwesome(imgPlan1, name: "calendaro", color: Colors.red)
        StyleHelper.setFontImageVisualsFontAwesome(imgPlan2, name: "clocko", color: Colors.red)
        
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: AppointmentModel){
        
        print(data.isStylist);
        if let salon = data.get_salon{
            
            UtilityHelper.setImage(imgVIew, salon.image);
            self.subHeading1.text = salon.name;
            self.subHeading2.text = data.duration;
            self.lblC1.text = data.date
            self.lblC2.text = data.time
            
        }
        
        if let stylist = data.get_stylist {
            
            UtilityHelper.setImage(imgVIew, stylist.image);
            self.subHeading1.text = stylist.name;
            self.subHeading2.text = data.duration;
            self.lblC1.text = data.date
            self.lblC2.text = data.time
            
        }
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
}
