//
//  PrivacyCVC.swift
//  salonistan
//
//  Created by Xtreme Hardware on 17/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class PrivacyCVC: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let img = StyleHelper.FontAwesomeImage(name: "lock", color: Colors.red, width: 20, height: 20)
        self.img.image = img;
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
