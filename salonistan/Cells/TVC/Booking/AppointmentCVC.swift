//
//  AppointmentCVC.swift
//  salonistan
//
//  Created by Xtreme Hardware on 17/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class AppointmentCVC: UITableViewCell {
    @IBOutlet weak var Service: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var Date: UILabel!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var Time: UILabel!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var Location: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let img1 = StyleHelper.FontAwesomeImage(name: "calendar", color: Colors.red, width: 20, height: 20)
        self.img1.image = img1;
        let img2 = StyleHelper.FontAwesomeImage(name: "clocko", color: Colors.red, width: 20, height: 20)
        self.img2.image = img2;
        let img3 = StyleHelper.FontAwesomeImage(name: "mapmarker", color: Colors.red, width: 20, height: 20)
        self.img3.image = img3;
        Date.setBorderBottom(color: Colors.red, height: 1)
        Time.setBorderBottom(color: Colors.red, height: 1)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
