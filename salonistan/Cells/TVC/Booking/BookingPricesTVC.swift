//
//  BookingPricesTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 18/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class BookingPricesTVC: UITableViewCell {

    
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var prices: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ data: AssociatedService){
        
        heading.text = data.service.name;
        prices.text = data.serviceCost;
        
        
    }
    
    func setData(total: String){
        
        heading.text = "Total"
        prices.text = total
        
        
    }
    
}
