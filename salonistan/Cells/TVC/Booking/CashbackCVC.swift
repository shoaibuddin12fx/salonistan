//
//  CashbackCVC.swift
//  salonistan
//
//  Created by Xtreme Hardware on 11/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class CashbackCVC: UITableViewCell {

    @IBOutlet weak var hdng: UILabel!
    @IBOutlet weak var subHdng: UILabel!
    @IBOutlet weak var subHdng1: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let img = StyleHelper.FontAwesomeImage(name: "exclamationtriangle", color: Colors.yellow, width: 20, height: 20)
        self.img.image = img;
        
        hdng.setBorderBottom(color: Colors.red, height: 1)

        label2.roundBorder(color: Colors.lightgray, width: 1)
        
        label2.setBackgroundColor(color: Colors.lightgray)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
