//
//  SearchTVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 16/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit

class SearchTVC: UITableViewCell {

    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var colorView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ service: ServiceModel){
        self.type.text = "Service";
        self.label.text = service.name ?? "";
        colorView.roundBorder(color: Colors.green, width: 1);
        colorView.backgroundColor = StyleHelper.colorWithHexString(Colors.green);
    }
    
    func setData(_ category: CategoryModel, number: Int){
        
        self.label.text = category.name ?? ""
        
        if(number == 0){
            self.type.text = "Services"
            colorView.roundBorder(color: Colors.green, width: 1);
            colorView.backgroundColor = StyleHelper.colorWithHexString(Colors.green);
        }else{
            self.type.text = "Category"
            colorView.roundBorder(color: Colors.brown, width: 1);
            colorView.backgroundColor = StyleHelper.colorWithHexString(Colors.brown);
        }
        
        
    }
    
    func setData(_ salon: SalonModel){
        self.type.text = "Salon"
        self.label.text = salon.name ?? ""
        colorView.roundBorder(color: Colors.blue, width: 1);
        colorView.backgroundColor = StyleHelper.colorWithHexString(Colors.blue);
    }
    
}
