//
//  BannerCVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 14/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit

class BannerCVC: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(name: String){
        
        UtilityHelper.setImage(imgView, name);
        
    }

}
