//
//  TestimonialCVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 25/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit

class TestimonialCVC: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var heading: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgView.roundBorder(color: Colors.white, width: 3)
    }
    
    func setData(_ data: [String:Any]){
        if let lbl = data["label"] as? String, let img = data["image"] as? String{
            let image = UIImage(named: img);
            self.imgView.image = image;
            self.heading.text = lbl;
        }
    }

}
