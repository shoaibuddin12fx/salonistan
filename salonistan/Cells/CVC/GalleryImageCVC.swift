//
//  GalleryImageCVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 01/01/2019.
//  Copyright © 2019 pixeltech. All rights reserved.
//

import UIKit

class GalleryImageCVC: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(_ image: String){
        UtilityHelper.setImage(imgView, image);
    }
    
    

}
