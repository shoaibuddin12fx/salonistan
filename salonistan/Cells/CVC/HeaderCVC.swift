//
//  HeaderCVC.swift
//  salonistan
//
//  Created by Osama Ahmed on 27/12/2018.
//  Copyright © 2018 pixeltech. All rights reserved.
//

import UIKit

class HeaderCVC: UICollectionViewCell {

    @IBOutlet weak var heading: UILabel!
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                //This block will be executed whenever the cell’s selection state is set to true (i.e For the selected cell);
                self.heading.textColor = StyleHelper.colorWithHexString(Colors.red);
                
            }
            else
            {
                //This block will be executed whenever the cell’s selection state is set to false (i.e For the rest of the cells)
                self.heading.textColor = StyleHelper.colorWithHexString(Colors.gray);
            }
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(_ name: String){
        self.heading.text = name.uppercased();
    }

}
